package au.com.wp.corp.gia.dto;



import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;




public class ConstraintModelTest {
	 
	ConstraintModel model = new ConstraintModel();
	List<Group> group = new ArrayList<Group>();
	List<VariableModel> variable = new ArrayList<VariableModel>();
	
	@Test
	public void constraintModelTest() {
		model.setCoefficient(new Double(.02));
		model.setCoefficientKey(new Long(1));
		model.setComm("comment1");
		model.setConstraintGrpKey(new Long[1]);
		model.setCreatedBy("Rudra");
		model.setCreatedTimestamp("02/02/2017");
		model.setCurrentHistoryKey(new Long(1));
		model.setDesc("description");
		model.setEndDate(new Date());
		model.setEqn("equation");
		model.setGroup(group);
		model.setGroupDescription("description");
		model.setId(new Long(1));
		model.setIsEnabled("T");
		model.setLastModified("03/02/2017");
		model.setLastModifiedBy("Rudra");
		model.setMirrorKey(new Long(1));
		model.setOper("operator");
		model.setPublishStatus("New");
		model.setRhs("RHS");
		model.setStartDate(new Timestamp(1));
		model.setVariable(variable);
		model.setVersion(new Long(1));
		model.getCoefficient();
		model.getCoefficientKey();
		model.getComm();
		model.getConstraintGrpKey();
		model.getCreatedBy();
		model.getCreatedTimestamp();
		model.getCurrentHistoryKey();
		model.getDesc();
		model.getEndDate();
		model.getEqn();
		model.getGroup();
		model.getGroupDescription();
		model.getId();
		model.getIsEnabled();
		model.getLastModified();
		model.getLastModifiedBy();
		model.getMirrorKey();
		model.getOper();
		model.getPublishStatus();
		model.getRhs();
		model.getStartDate();
		model.getVariable();
		model.getVersion();
		
	}
	
}
