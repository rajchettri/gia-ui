package au.com.wp.corp.gia.domain;

import java.sql.Timestamp;

import org.junit.Test;

public class LHSCoefficientTest {

	LHSCoefficient coeff = new LHSCoefficient();
	ConstraintEquation constraint = new ConstraintEquation();
	@Test
	public void defaultPageTest() {
		coeff.setCoefficient(new Double(.03));
		coeff.setConstraintEquation(constraint);
		coeff.setCurrentHistoryKey(new Long(1));
		coeff.setLastModifiedBy("Rudra");
		coeff.setLastModifiedTimeStamp(new Timestamp(1));
		coeff.setVariableKey(new Long(1));
		coeff.getCoefficient();
		coeff.getConstraintEquation();
		coeff.getCurrentHistoryKey();
		coeff.getLastModifiedBy();
		coeff.getLastModifiedTimeStamp();
		coeff.getVariableKey();
	}
}
