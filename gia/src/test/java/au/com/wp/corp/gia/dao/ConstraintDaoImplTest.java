package au.com.wp.corp.gia.dao;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.service.jdbc.connections.internal.C3P0ConnectionProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.wp.corp.gia.domain.ConstraintEquation;
import au.com.wp.corp.gia.domain.ConstraintGroup;
import au.com.wp.corp.gia.domain.Variable;
import au.com.wp.corp.gia.dto.ConstraintModel;
@RunWith(MockitoJUnitRunner.class)
public class ConstraintDaoImplTest {
	
	ConstraintModel model = new ConstraintModel();
	
	
	@Mock
	SessionFactory sessionFactory;
	@Mock
	Session session;
	@Mock
	C3P0ConnectionProvider provider;
	@Mock
	Query query;
	
	@InjectMocks
	ConstraintDao daoImpl = new ConstraintDaoImpl();
	
	@Before
	public void setUP(){
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	}
	
	@Test
	public void insertConstraintTest() throws ParseException{
		model.setIsEnabled("T");
		String name = "n040046";
		daoImpl.insertConstraint(model,name);
	}
	
	@Test
	public void insertConstraintTestStartDate() throws ParseException{
		model.setIsEnabled("T");
		model.setComm("comment");
		String name = "n040046";
		model.setStartDate(new java.sql.Timestamp(System.currentTimeMillis()));
		daoImpl.insertConstraint(model,name);
	}
	
	
	@Test
	public void insertConstraintTestEndDate() throws ParseException{
		model.setIsEnabled("T");
		String name = "n040046";
		model.setEndDate(new java.sql.Timestamp(System.currentTimeMillis()));
		daoImpl.insertConstraint(model,name);
	}
	
	//@Test
	public void retrieveConstraintTest(){
		Mockito.when(session.createQuery("from ConstraintGroup ")).thenReturn(query);
		List<ConstraintGroup> list = new ArrayList<ConstraintGroup>();
		ConstraintGroup consGrp = new ConstraintGroup();
		consGrp.setGroup("group");
		list.add(consGrp);
		Mockito.when(query.list()).thenReturn(list);
		daoImpl.retrieveConstraint();
	}
	
	//@Test
	public void retrieveConstraintvarTest(){
		Mockito.when(session.createQuery("from Variable ")).thenReturn(query);
		List<Variable> list = new ArrayList<Variable>();
		Variable consGrp = new Variable();
		consGrp.setAlias("alias");
		
		list.add(consGrp);
		Mockito.when(query.list()).thenReturn(list);
		daoImpl.retrieveConstraint();
	}
	
	//@Test
	public void retrieveAllConstraintTest(){
		Mockito.when(session.createQuery("from ConstraintEquation ")).thenReturn(query);
		List<ConstraintEquation> list = new ArrayList<ConstraintEquation>();
		ConstraintEquation equation = new ConstraintEquation();
		equation.setEqn("test");
		equation.setVersion(new Long(1));
		equation.setId(new Long(1));
		equation.setDesc("desc");
		equation.setLastModifiedBy("02/02/2017");
		equation.setLastModifiedTimeStamp(new Timestamp(1));
		equation.setCreatedBy("Rudra");
		equation.setCreatedTimestamp(new Timestamp(1));
		equation.setIsEnabled("T");
		equation.setStartDate(new Timestamp(1));
		equation.setEndDate(new Timestamp(1));
		equation.setPublishStatus("NEW");
		equation.setLastEnabled(new Timestamp(1));
		equation.setLastDisabled(new Timestamp(1));
		
		list.add(equation);
		Mockito.when(query.list()).thenReturn(list);
		daoImpl.retrieveAllConstraint();
	}
}
