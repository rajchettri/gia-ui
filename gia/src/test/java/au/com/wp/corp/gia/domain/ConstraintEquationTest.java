/**
 * 
 */
/**
 * @author n040046
 *
 */
package au.com.wp.corp.gia.domain;





import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ch.qos.logback.core.joran.action.TimestampAction;



public class ConstraintEquationTest {

	ConstraintEquation constraint = new ConstraintEquation();
	Set<ConstraintToGroup> constraintTogrp = new HashSet<ConstraintToGroup>();
	Set<LHSCoefficient> lhsCoeffList = new HashSet<LHSCoefficient>();
	
	@Test
	public void defaultPageTest() {
		constraint.setComm("comment");
		constraint.setConstraintToGroupList(constraintTogrp);
		constraint.setCreatedBy("Rudra");
		constraint.setCreatedTimestamp(new Timestamp(1));
		constraint.setCurrentHistoryKey(new Long(1));
		constraint.setDesc("description");
		constraint.setEndDate(new Timestamp(1));
		constraint.setEqn("equation");
		constraint.setId(new Long(1));
		constraint.setIsEnabled("T");
		constraint.setLastModifiedBy("Rudra");
		constraint.setLastModifiedTimeStamp(new Timestamp(1));
		constraint.setLhsList(lhsCoeffList);
		constraint.setMirrorKey(new Long(1));
		constraint.setOper("oretation");
		constraint.setPublishStatus("NEW");
		constraint.setRhs("RHS");
		constraint.setStartDate(new Timestamp(1));
		constraint.setVersion(new Long(1));
		constraint.getComm();
		constraint.getLhsList();
		constraint.getPublishStatus();
		constraint.getConstraintToGroupList();
		constraint.getCreatedBy();
		constraint.getCreatedTimestamp();
		constraint.getCurrentHistoryKey();
		constraint.getDesc();
		constraint.getEndDate();
		constraint.getEqn();
		constraint.getId();
		constraint.getIsEnabled();
		constraint.getLastModifiedBy();
		constraint.getLastModifiedTimeStamp();
		constraint.getRhs();
		constraint.getVersion();
		constraint.getOper();
		constraint.getMirrorKey();
		constraint.getStartDate();
	}

	
	
	
}