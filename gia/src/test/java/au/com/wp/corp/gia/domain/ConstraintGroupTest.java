package au.com.wp.corp.gia.domain;


import java.sql.Timestamp;
import java.util.HashSet;

import java.util.Set;

import org.junit.Test;

public class ConstraintGroupTest {
	ConstraintGroup group = new ConstraintGroup();
	Set<ConstraintToGroup> grpList = new HashSet<ConstraintToGroup>();
	@Test
	public void defaultPageTest() {
		group.setConstraintToGroupList(grpList);
		group.setGroup("group");
		group.setGroupDescription("description");
		group.setHistoryKey(new Long(1));
		group.setModifiedBy("Rudra");
		group.setModifiedTimeStamp(new Timestamp(1));
		group.getConstraintToGroupList();
		group.getGroup();
		group.getGroupDescription();
		group.getHistoryKey();
		group.getId();
		group.getModifiedBy();
		group.getModifiedTimeStamp();
	}
}
