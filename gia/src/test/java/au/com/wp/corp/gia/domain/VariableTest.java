/**
 * 
 */
/**
 * @author n040046
 *
 */
package au.com.wp.corp.gia.domain;



import org.mockito.Mock;

import java.sql.Timestamp;

import org.junit.Test;



public class VariableTest {

	Variable variable = new Variable();
	
	
	@Test
	public void defaultPageTest() {

		variable.setAlias("alias");
		variable.setGiaFlag("T");
		variable.setHistoryKey(new Long(1));
		variable.setModifiedBy("RUDRA");
		variable.setModifiedTimestamp(new Timestamp(1));
		variable.setName("Rudra");
		variable.getAlias();
		variable.getGiaFlag();
		variable.getHistoryKey();
		variable.getId();
		variable.getModifiedBy();
		variable.getModifiedTimestamp();
		variable.getName();
	}

	
	
	
}