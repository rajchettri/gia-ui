package au.com.wp.corp.gia.web.login.controller;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import au.com.wp.corp.gia.dao.ConstraintDao;
import au.com.wp.corp.gia.dao.ConstraintDaoImpl;
import au.com.wp.corp.gia.dto.*;
import au.com.wp.corp.gia.web.controller.ConstraintManagementController;

import org.junit.Before;

@RunWith(MockitoJUnitRunner.class)

public class ConstraintManagementControllerTest {

	ConstraintManagementController controller = new ConstraintManagementController();
	@InjectMocks
	ConstraintDao constrainDao=new ConstraintDaoImpl();
	
	@Mock
	HttpServletRequest request;
	
	@Mock
	SessionFactory sessionFactory;
	
	@Mock
	Session session;
	
	@Mock
	Query query;
	
	
	@Mock
	SecurityContext context1;
	
	
	
	@Before
	public void setUP(){
		
		Mockito.when(sessionFactory.getCurrentSession()).thenReturn(session);
	}
	
	
	@Mock
	ModelMap model;
	
	@Mock
	Authentication authentication;
	
	@Mock
	SecurityContextHolder context;
	
	@Test
	public void defaultPageTest() {

		//controller.defaultPage();

	}

	
	
	public void welcomePageTest() {
		//Mockito.when(context.getContext()).thenReturn(context1);
		Mockito.when(context1.getAuthentication()).thenReturn(authentication);
		//controller.welcomePage(model);

	}
	
	
	@Test
	public void loginTest(){
		//controller.login("error", "logout");
	}
	
	@Test
	public void loginTestLogout(){
		//controller.login(null, "logout");
	}
	
	@Test
	public void loginTestAllNull(){
		//controller.login(null, null);
	}
	
	
	@Test
	public void loadPageTest(){
		List list = new ArrayList();
	Group group =new Group();
		Mockito.when(constrainDao.retrieveAllConstraint()).thenReturn(list);
		controller.loadPage(group, request);
	}


	
	
}