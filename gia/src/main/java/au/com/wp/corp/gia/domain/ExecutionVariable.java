package au.com.wp.corp.gia.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;


@Entity
@Table(name="EXECUTION_VARIABLE")
public class ExecutionVariable {

	
	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "EXECUTION_VARIABLE_KEY", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	@Column(name = "EXECUTION_VARIABLE_KEY", nullable = false)
	private Long id;
	
	@Column(name = "MODEL_EXECUTION_KEY", nullable = false)
	private Long modelExecutionKey;
	
	@Column(
	        name="VARIABLE_HISTORY_KEY",
	        nullable=false,
	        unique=true,
	        columnDefinition = "NUMBER DEFAULT nextval('VARIABLE_HISTORY_KEY')"
	)
	@Generated(GenerationTime.INSERT)
	private Long variableHistoryKey;
	
	@Column(name = "LHS_INPUT_VALUE", nullable = false)
	private Long LHSInputValue;
	
	@Column(name = "RHS_INPUT_VALUE", nullable = false)
	private Long RHSInputValue;
	
	@Column(name = "LHS_OUTPUT_VALUE", nullable = false)
	private Long LHSOutputValue;
	
	@Column(name = "LHS_SOURCE_FLAG", nullable = false)
	private String LHSSourceFlag;

	@Column(name = "OPERATING_INSTRUCTION_ID", nullable = false)
	private String operatingInstructId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getModelExecutionKey() {
		return modelExecutionKey;
	}

	public void setModelExecutionKey(Long modelExecutionKey) {
		this.modelExecutionKey = modelExecutionKey;
	}

	public Long getVariableHistoryKey() {
		return variableHistoryKey;
	}

	public void setVariableHistoryKey(Long variableHistoryKey) {
		this.variableHistoryKey = variableHistoryKey;
	}

	public Long getLHSInputValue() {
		return LHSInputValue;
	}

	public void setLHSInputValue(Long lHSInputValue) {
		LHSInputValue = lHSInputValue;
	}

	public Long getRHSInputValue() {
		return RHSInputValue;
	}

	public void setRHSInputValue(Long rHSInputValue) {
		RHSInputValue = rHSInputValue;
	}

	public Long getLHSOutputValue() {
		return LHSOutputValue;
	}

	public void setLHSOutputValue(Long lHSOutputValue) {
		LHSOutputValue = lHSOutputValue;
	}

	public String getLHSSourceFlag() {
		return LHSSourceFlag;
	}

	public void setLHSSourceFlag(String lHSSourceFlag) {
		LHSSourceFlag = lHSSourceFlag;
	}

	public String getOperatingInstructId() {
		return operatingInstructId;
	}

	public void setOperatingInstructId(String operatingInstructId) {
		this.operatingInstructId = operatingInstructId;
	}
}
