package au.com.wp.corp.gia.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


public class ConstraintModel {
	 
	private Long id;

	private String eqn;

	private Long version;

	private String desc;

	private String isEnabled;
	
	@JsonFormat(pattern = "yyyy/MM/dd")
	private Date startDate;
	
	@JsonFormat(pattern = "yyyy/MM/dd")
	private Date endDate;
	
	private String startDate1;
	
	private String endDate1;

	private Long mirrorKey;

	private String rhs;

	private String oper;

	private String publishStatus;

	private String createdTimestamp;
	
	private String comm;

	private String createdBy;

	private Long currentHistoryKey;
	
	
	 private Object[] constraintGrpKey;
	 
	 //added for handling LHS coefficient
	// @JsonProperty("gia.coefficientKeyValue")
	 private Object[] coefficientKeyValue;
	 
	 private Long coefficientKey;
	
	 private String lastEnabled;
	 
	 private String lastEnabledBy;
	 
	 private String lastDisabledBy;
	 
	@JsonProperty("gia")
	private List<Group> group;
	
	@JsonProperty("CoeffKey")
	private List<VariableModel> variable;
	
	private String groupDescription;

	private Double coefficient;
	
	private String variableName;
	
	private Long LHSKey;
	
	private String lastModifiedBy;
	
	private String lastModified;
	
	private String lastDisabled;
	
	//added to diaplay group  (, seperated)
	private String constraintGrpName;

	private String constraintGrpId;
	
	private String isCEpresent;
	
	String statusMsg;
	
	
	
	public ConstraintModel(){
		
	}
	
	public ConstraintModel(String Id){
		
		this.setId(new Long(Id));
	}
	
		
	
	public String getIsCEpresent() {
		return isCEpresent;
	}

	public void setIsCEpresent(String isCEpresent) {
		this.isCEpresent = isCEpresent;
	}

	public String getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(String createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	
	public String getLastDisabled() {
		return lastDisabled;
	}

	public void setLastDisabled(String lastDisabled) {
		this.lastDisabled = lastDisabled;
	}

	public String getEqn() {
		return eqn;
	}

	public void setEqn(String eqn) {
		this.eqn = eqn;
	}

	public Long getVersion() {
		return version;
	}

	
	public String getLastEnabled() {
		return lastEnabled;
	}

	public void setLastEnabled(String lastEnabled) {
		this.lastEnabled = lastEnabled;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getRhs() {
		return rhs;
	}

	public void setRhs(String rhs) {
		this.rhs = rhs;
	}

	

	

	public String getConstraintGrpId() {
		return constraintGrpId;
	}

	public void setConstraintGrpId(String constraintGrpId) {
		this.constraintGrpId = constraintGrpId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getMirrorKey() {
		return mirrorKey;
	}

	public void setMirrorKey(Long mirrorKey) {
		this.mirrorKey = mirrorKey;
	}

	

	

	public String getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(String publishStatus) {
		this.publishStatus = publishStatus;
	}

	

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCurrentHistoryKey() {
		return currentHistoryKey;
	}

	public void setCurrentHistoryKey(Long currentHistoryKey) {
		this.currentHistoryKey = currentHistoryKey;
	}

	

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	


	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	

	

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public List<Group> getGroup() {
		return group;
	}

	public void setGroup(List<Group> group) {
		this.group = group;
	}

	
	

	public Object[] getConstraintGrpKey() {
		return constraintGrpKey;
	}

	public void setConstraintGrpKey(Object[] constraintGrpKey) {
		this.constraintGrpKey = constraintGrpKey;
	}

	public Long getCoefficientKey() {
		return coefficientKey;
	}

	public void setCoefficientKey(Long coefficientKey) {
		this.coefficientKey = coefficientKey;
	}

	public List<VariableModel> getVariable() {
		return variable;
	}

	public void setVariable(List<VariableModel> variable) {
		this.variable = variable;
	}

	public String getConstraintGrpName() {
		return constraintGrpName;
	}

	public void setConstraintGrpName(String constraintGrpName) {
		this.constraintGrpName = constraintGrpName;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public Long getLHSKey() {
		return LHSKey;
	}

	public void setLHSKey(Long lHSKey) {
		LHSKey = lHSKey;
	}

	public Object[] getCoefficientKeyValue() {
		return coefficientKeyValue;
	}

	public void setCoefficientKeyValue(Object[] coefficientKeyValue) {
		
		this.coefficientKeyValue = coefficientKeyValue;
	}

	public String getLastEnabledBy() {
		return lastEnabledBy;
	}

	public void setLastEnabledBy(String lastEnabledBy) {
		this.lastEnabledBy = lastEnabledBy;
	}

	public String getLastDisabledBy() {
		return lastDisabledBy;
	}

	public void setLastDisabledBy(String lastDisabledBy) {
		this.lastDisabledBy = lastDisabledBy;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public String getStartDate1() {
		return startDate1;
	}

	public void setStartDate1(String startDate1) {
		this.startDate1 = startDate1;
	}

	public String getEndDate1() {
		return endDate1;
	}

	public void setEndDate1(String endDate1) {
		this.endDate1 = endDate1;
	}

	
	

}
