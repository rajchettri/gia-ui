/**
 * 
 */
package au.com.wp.corp.gia.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.wp.corp.gia.dto.User;
import au.com.wp.corp.gia.util.GiaConstants;

/**
 * @author N039126
 *
 */
@Controller
public class HomeController {

	private static Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping("/error")
	public String error(ModelMap model) {
		model.addAttribute("error", "true");
		return "login";

	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/logout")
	public String logout() {
		return "login";
	}
	
	@RequestMapping(value={"/","/home"} )
	public String home() {
		System.out.println("Inside redirection method-----");
		return "index";
		//return "constraintManagement";
	}
	
	@RequestMapping(value="/userdetails")
	public @ResponseBody User  getUserDetails(){
		
		logger.info("User Role access :----");
		User user = new User();
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().
				   getAuthentication().getPrincipal();
		user.setPassword(userDetails.getPassword());
		user.setUsername(userDetails.getUsername());
		
		List<String> userRoles = new ArrayList<String>();
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)userDetails.getAuthorities();
		for(GrantedAuthority auth :authorities){
			userRoles.add(auth.getAuthority());
			logger.info("User Role : "+auth.getAuthority());
		}
		String role = null;
		
		if(userRoles.size()>1){
			role = "BOTH";
		} else {
			if(GiaConstants.GIA_OPSUI_RW.equalsIgnoreCase(userRoles.get(0)) 
					|| GiaConstants.GIA_OPSUI_RO.equalsIgnoreCase(userRoles.get(0))){
				role="OPER";
			}else if(GiaConstants.GIA_WHATIF_RO.equalsIgnoreCase(userRoles.get(0)) 
					|| GiaConstants.GIA_WHATIF_RW.equalsIgnoreCase(userRoles.get(0))){
				role="TEST";
			}
		}
		user.setUserRole(role);
		logger.info("User Role :-"+role);
		
		return user;
	}
}
