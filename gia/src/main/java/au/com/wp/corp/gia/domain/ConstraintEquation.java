package au.com.wp.corp.gia.domain;

import static au.com.wp.corp.gia.domain.DbConstants.C_EQUATION_KEY;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "CONSTRAINT_EQUATION")

public class ConstraintEquation {
	@Id
	@GeneratedValue(generator = "CONSTRAINT_EQUATION_KEY")
	@GenericGenerator(strategy = "sequence", name = "CONSTRAINT_EQUATION_KEY", parameters = {
			@Parameter(name = "sequence", value = "CONSTRAINT_EQUATION_KEY") })
	@Column(name = C_EQUATION_KEY, nullable = false)
	private Long Id;

	@Column(name = "NAME", nullable = false, length = 256)
	private String eqn;

	@Column(name = "VERSION", nullable = false)
	private Long version;

	@Column(name = "CONSTRAINT_DESCRIPTION", nullable = false, length = 4000)
	private String desc;
	
	@Column(name = "ENABLED_FLAG", nullable = false)
	private String isEnabled;
	
	@Column(name="LAST_DISABLED_TIMESTAMP", nullable=true)
	private Timestamp lastDisabled;
	
	@Column(name = "EFFECTIVE_FROM_DATE",nullable = false)
	private Timestamp startDate;
	
	@Column(name = "EFFECTIVE_TO_DATE", nullable = false)
	private Timestamp endDate;

	@Column(name = "MIRROR_KEY", nullable = false)
	private Long mirrorKey;

	@Column(name = "RHS_EQUATION", nullable = false)
	private String rhs;

	@Column(name = "CONSTRAINT_OPERATOR", nullable = false)
	private String oper;

	@Column(name = "PUBLISH_STATUS", nullable = false)
	private String publishStatus;

	@Column(name = "CREATED_TIMESTAMP", nullable = false)
	private Timestamp createdTimestamp;

	@Column(name = "CREATED_BY", nullable = false)
	private String createdBy;
	
	@Column(
	        name="CURRENT_HISTORY_KEY",
	        nullable=false,
	        unique=true,
	        columnDefinition = "NUMBER DEFAULT nextval('CONSTRAINT_EQUATION_HIST_KEY')"
	)
	@Generated(GenerationTime.INSERT)
	private Long currentHistoryKey;

	@Column(name="LAST_MODIFIED_BY")
	private String lastModifiedBy;
	
	@Column(name="LAST_MODIFIED_TIMESTAMP")
	private Timestamp lastModifiedTimeStamp;
	
	@Column (name="CHANGE_COMMENT",nullable = true)
	private String comm;
	
	@Column(name = "LAST_ENABLED_TIMESTAMP", nullable = true)
	private Timestamp lastEnabled;
	
	@Column(name = "LAST_ENABLED_BY", nullable = true)
	private String lastEnabledBy;
	
	@Column(name="LAST_DISABLED_BY")
	private String lastDisabledBy;

	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "constraintEq")
	private Set<ConstraintToGroup> constraintToGroupList;

	/*
	 * added for LHS coefficient
	 */

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "constraintEquation")
	private Set<LHSCoefficient> lhsList;

	public ConstraintEquation(){
		
	}
	
	public ConstraintEquation(String Id){
		System.out.println("ConstraintEquation Id="+Id);
		this.setId(new Long(Id));
	}
	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public String getRhs() {
		return rhs;
	}

	public void setRhs(String rhs) {
		this.rhs = rhs;
	}

	public Set<LHSCoefficient> getLhsList() {
		return lhsList;
	}

	public void setLhsList(Set<LHSCoefficient> stockDailyRecords) {
		this.lhsList = stockDailyRecords;
	}

	
	public Timestamp getLastDisabled() {
		return lastDisabled;
	}

	public void setLastDisabled(Timestamp lastDisabled) {
		this.lastDisabled = lastDisabled;
	}

	public Timestamp getLastEnabled() {
		return lastEnabled;
	}

	public void setLastEnabled(Timestamp lastEnabled) {
		this.lastEnabled = lastEnabled;
	}

	public Long getCurrentHistoryKey() {
		return currentHistoryKey;
	}

	public void setCurrentHistoryKey(Long currentHistoryKey) {
		this.currentHistoryKey = currentHistoryKey;
	}

	public String getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(String publishStatus) {
		this.publishStatus = publishStatus;
	}

	public Timestamp getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public Long getMirrorKey() {
		return mirrorKey;
	}

	public void setMirrorKey(Long mirrorKey) {
		this.mirrorKey = mirrorKey;
	}

	
	public String getEqn() {
		return eqn;
	}

	public void setEqn(String eqn) {
		this.eqn = eqn;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Set<ConstraintToGroup> getConstraintToGroupList() {
		return constraintToGroupList;
	}

	public void setConstraintToGroupList(Set<ConstraintToGroup> constraintToGroupList) {
		this.constraintToGroupList = constraintToGroupList;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Timestamp getLastModifiedTimeStamp() {
		return lastModifiedTimeStamp;
	}

	public void setLastModifiedTimeStamp(Timestamp lastModifiedTimeStamp) {
		this.lastModifiedTimeStamp = lastModifiedTimeStamp;
	}

	public String getLastEnabledBy() {
		return lastEnabledBy;
	}

	public void setLastEnabledBy(String lastEnabledBy) {
		this.lastEnabledBy = lastEnabledBy;
	}

	public String getLastDisabledBy() {
		return lastDisabledBy;
	}

	public void setLastDisabledBy(String lastDisabledBy) {
		this.lastDisabledBy = lastDisabledBy;
	}

	

}
