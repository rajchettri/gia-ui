package au.com.wp.corp.gia.util;

/**
 * @author n039804
 *
 */
public class GiaConstants {

	
	/**
	 * User Roles :
	 */
	public static String  GIA_OPSUI_RO = "ROLE_GIA_OPSUI_RO";
	public static String  GIA_OPSUI_RW = "ROLE_GIA_OPSUI_RW";
	public static String GIA_WHATIF_RO = "ROLE_GIA_WHATIF_RO";
	public static String GIA_WHATIF_RW = "ROLE_GIA_WHATIF_RW";
	public static String   GIA_PUBLISH = "ROLE_GIA_PUBLISH";
	
	/**
	 * GIA Constants:
	 * 
	 * 
	 */
	public static String NO_APPLICABLE="NA";
	public static final String SIMULATED = "Simulated";
	public static final String DISABLED = "Disabled";
	public static final String ENABLED = "Enabled";
	public static final String KEY_S = "S";
	public static final String KEY_E = "E";
	public static final String KEY_D = "D";

}
