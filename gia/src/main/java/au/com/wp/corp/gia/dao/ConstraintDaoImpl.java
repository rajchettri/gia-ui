package au.com.wp.corp.gia.dao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import au.com.wp.corp.gia.domain.ConstraintEquation;
import au.com.wp.corp.gia.domain.ConstraintGroup;
import au.com.wp.corp.gia.domain.ConstraintToGroup;
import au.com.wp.corp.gia.domain.LHSCoefficient;
import au.com.wp.corp.gia.domain.Variable;
import au.com.wp.corp.gia.dto.ConstraintModel;
import au.com.wp.corp.gia.dto.Group;
import au.com.wp.corp.gia.dto.VariableModel;
import au.com.wp.corp.gia.util.GiaConstants;

@Component
public class ConstraintDaoImpl implements ConstraintDao {
	
	
	private static Logger logger = LoggerFactory.getLogger(ConstraintDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	public ConstraintDaoImpl() {

	}

	private class Status {
		private Long count;
		private String eqn;
		private Long Id;
		private Long version;

		public Long getCount() {
			return count;
		}

		public void setCount(Long count) {
			this.count = count;
		}

		public String getEqn() {
			return eqn;
		}

		public void setEqn(String eqn) {
			this.eqn = eqn;
		}

		public Long getId() {
			return Id;
		}

		public void setId(Long id) {
			Id = id;
		}

		public Long getVersion() {
			return version;
		}

		public void setVersion(Long version) {
			this.version = version;
		}

	}

	Status status = new Status();

	@Transactional
	@Override
	public void insertConstraint(ConstraintModel constraintModel, String name) throws ParseException {

		Session session = sessionFactory.getCurrentSession();

		ConstraintEquation constraint = new ConstraintEquation();

		constraint.setEqn(constraintModel.getEqn());

		constraint.setDesc(constraintModel.getDesc());
		if (constraintModel.getIsEnabled() != null && !constraintModel.getIsEnabled().equals("")) {
			if (constraintModel.getIsEnabled().equals("true")) {
				constraint.setIsEnabled("T");
				constraint.setLastEnabledBy(name);
				constraint.setLastEnabled(new Timestamp(System.currentTimeMillis()));
			} else {
				constraint.setIsEnabled("F");
				constraint.setLastDisabledBy(name);
				constraint.setLastDisabled(new Timestamp(System.currentTimeMillis()));
			}
		} else {
			constraint.setLastDisabledBy(name);
			constraint.setLastDisabled(new Timestamp(System.currentTimeMillis()));
			constraint.setIsEnabled("F");
		}

		if (constraintModel.getStartDate() != null) {
			constraint.setStartDate(new java.sql.Timestamp(constraintModel.getStartDate().getTime()));
		} else {
			constraint.setStartDate(new java.sql.Timestamp(System.currentTimeMillis()));
		}
		if (constraintModel.getEndDate() != null) {

			constraint.setEndDate(new java.sql.Timestamp(constraintModel.getEndDate().getTime()));
		} else {
			DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Date d = formatter.parse("2099/12/31");
			((SimpleDateFormat) formatter).applyPattern("yyyy-MM-dd HH:mm:ss.SSS");
			String newDateString = formatter.format(d);
			Timestamp ts = Timestamp.valueOf(newDateString);
			constraint.setEndDate(ts);
		}
		constraint.setMirrorKey(new Long(1));
		constraint.setRhs(constraintModel.getRhs());
		if (constraintModel.getComm() != null)
			constraint.setComm(constraintModel.getComm());
		constraint.setOper(constraintModel.getOper());
		if (constraintModel.getVersion() != null) {
			Long version = constraintModel.getVersion();
			constraint.setVersion(version + 1);
			constraintModel.setVersion(version + 1);
		} else {
			constraint.setVersion(new Long(1));
			constraintModel.setVersion(new Long(1));
		}

		constraint.setPublishStatus("NEW");

		constraint.setLastModifiedBy(name);

		constraint.setCreatedTimestamp(new Timestamp(System.currentTimeMillis()));
		constraint.setCreatedBy(name);
		constraint.setCurrentHistoryKey(new Long(1));

		session.save(constraint);
		int length = constraintModel.getConstraintGrpKey().length;
		Object[] o = constraintModel.getConstraintGrpKey();
		for (int i = 0; i < length; i++) {
			ConstraintToGroup constraintToGroup = new ConstraintToGroup();

			constraintToGroup.setConstraintEq(constraint);
			constraintToGroup.setModifiedTimeStamp(new Timestamp(System.currentTimeMillis()));
			constraintToGroup.setModifiedBy(name);
			constraintToGroup.setHistoryKey(new Long(1));
			Map o1 = (HashMap) o[i];

			int constraintKey = (Integer) o1.get("constraintGroupKey");
			constraintToGroup.setConstraintGroupKey(new Long(constraintKey));

			session.save(constraintToGroup);

		}

		Object[] coefficientKeyValue = constraintModel.getCoefficientKeyValue();

		int lengthCoefficientKeyValue = coefficientKeyValue.length;
		for (int j = 0; j < lengthCoefficientKeyValue; j++) {
			LHSCoefficient lhsCoeff = new LHSCoefficient();

			lhsCoeff.setVariableKey(constraintModel.getCoefficientKey());

			Map map = (HashMap) coefficientKeyValue[j];
			Double value = new Double(map.get("coefficient").toString());
			lhsCoeff.setCoefficient(new BigDecimal(value).setScale(5,RoundingMode.HALF_UP).doubleValue());
			lhsCoeff.setVariableKey(new Long(map.get("lhskey").toString()));

			lhsCoeff.setLastModifiedTimeStamp(new Timestamp(100));
			lhsCoeff.setLastModifiedBy(name);
			lhsCoeff.setCurrentHistoryKey(new Long(1));
			lhsCoeff.setConstraintEquation(constraint);

			long cnt = (Long) session.save(lhsCoeff);

			status.setCount(cnt);
			status.setEqn(constraint.getEqn());
			status.setId(constraint.getId());
		}

	}

	@Transactional
	@Override
	public ConstraintModel retrieveConstraint() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ConstraintGroup ");
		List<Group> listGroup = new ArrayList<Group>();
		List list = query.list();
		Iterator<ConstraintGroup> iter = list.iterator();
		ConstraintModel constraint = new ConstraintModel();
		while (iter.hasNext()) {
			ConstraintGroup group = iter.next();
			Group model = new Group();
			model.setGroup(group.getGroup());
			if (group.getId() != null)
				model.setConstraintGroupKey(group.getId());
			listGroup.add(model);
		}

		Query query2 = session.createQuery("from Variable where giaFlag!=:flag");
		query2.setParameter("flag", GiaConstants.NO_APPLICABLE);
		List listVar = query2.list();
		List list4 = new ArrayList<VariableModel>();
		Iterator<Variable> iter2 = listVar.iterator();
		while (iter2.hasNext()) {
			Variable var = iter2.next();
			VariableModel model1 = new VariableModel();

			model1.setVariable(var.getAlias());
			if (var.getId() != null)
				model1.setLhskey(var.getId());
			list4.add(model1);
		}
		constraint.setGroup(listGroup);
		constraint.setVariable(list4);
		return constraint;
	}

	@Transactional
	public List retrieveAllConstraint() {
		Session session = sessionFactory.getCurrentSession();
		/*
		 * for published Query query1 =
		 * session.createQuery("select publishStatus from ConstraintEquation ");
		 * List<String> rows = query1.list(); for (String temp : rows){
		 * if(temp!=null){ if(temp.equalsIgnoreCase("PUB")){ Query query =
		 * session.
		 * createQuery("update ConstraintEquation set publishStatus = :status" +
		 * " where Id = :Id"); query.setParameter("status", "DEL");
		 * 
		 * //query.setParameter("Id", model.getId()); } } }
		 */
		Query query = session.createQuery("from ConstraintEquation ");

		List list = query.list();

		Iterator<ConstraintEquation> iter = list.iterator();
		List list2 = new ArrayList();
		while (iter.hasNext()) {
			ConstraintEquation equation = iter.next();
			ConstraintModel constraint = new ConstraintModel();
			constraint.setEqn(equation.getEqn());
			constraint.setVersion(equation.getVersion());
			constraint.setId(equation.getId());
			constraint.setDesc(equation.getDesc());
			constraint.setLastModifiedBy(equation.getLastModifiedBy());
			DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			String lastModified = formatter.format(equation.getLastModifiedTimeStamp());

			constraint.setLastModified(lastModified);
			constraint.setCreatedBy(equation.getCreatedBy());
			String createdTimestamp = formatter.format(equation.getCreatedTimestamp());
			constraint.setCreatedTimestamp(createdTimestamp);
			constraint.setIsEnabled(equation.getIsEnabled());
			constraint.setStartDate(equation.getStartDate());
			constraint.setEndDate(equation.getEndDate());
			constraint.setPublishStatus(equation.getPublishStatus());
			if (equation.getLastEnabled() != null) {
				String lastEnabled = formatter.format(equation.getLastEnabled());
				constraint.setLastEnabled(lastEnabled);
				constraint.setLastEnabledBy(equation.getLastEnabledBy());
			}

			if (equation.getLastDisabled() != null) {
				constraint.setLastDisabledBy(equation.getLastDisabledBy());
				String lastDisabled = formatter.format(equation.getLastDisabled());
				constraint.setLastDisabled(lastDisabled);
			}
			// code to retrive group
			Query queryConstraintToGroup = session
					.createQuery("from ConstraintToGroup where constraintEq = :constraintEq ");
			queryConstraintToGroup.setParameter("constraintEq", equation);
			List listConstraintToGroup = queryConstraintToGroup.list();
			
			Iterator<ConstraintToGroup> iteratorConstraintToGroup = listConstraintToGroup.iterator();
			SortedSet sf = new TreeSet();
			StringBuffer sf2 = new StringBuffer("");
			Object[] groupObj = new Object[listConstraintToGroup.size()];
			int counter = 0;
			while (iteratorConstraintToGroup.hasNext()) {

				ConstraintToGroup constraintToGrp = iteratorConstraintToGroup.next();
				Long constrantGrpKey = constraintToGrp.getConstraintGroupKey();
				Query queryConstraintGroup = session.createQuery("from ConstraintGroup where Id = :Id ");
				queryConstraintGroup.setParameter("Id", constrantGrpKey);
				List listConstraintGroup = queryConstraintGroup.list();
				Iterator<ConstraintGroup> iteratorConstraintGroup = listConstraintGroup.iterator();
				
				while (iteratorConstraintGroup.hasNext()) {
					ConstraintGroup constraintGroup = iteratorConstraintGroup.next();
					Group group = new Group();
					group.setGroup(constraintGroup.getGroup());
					
					String temp=constraintGroup.getGroup();

					sf.add(temp);
					group.setConstraintGroupKey(constraintGroup.getId());
					sf2.append(constraintGroup.getId());
					sf2.append(",");
					groupObj[counter] = group;
					counter++;
				}

				constraint.setConstraintGrpKey(groupObj);
				
				String str=sf.toString();
				String str2 = str.substring(1,str.length()-1);
				constraint.setConstraintGrpName(str2);
				
			}
			
			// code to retrive LHS coeff
			Object[] coefficientKeyValue = null;
			int countVar = 0;
			Query queryLHS = session.createQuery("from LHSCoefficient where constraintEquation = :constraintEquation ");
			queryLHS.setParameter("constraintEquation", equation);
			List listLhs = queryLHS.list();
			Iterator<LHSCoefficient> iteratorLhs = listLhs.iterator();
			StringBuffer sfVar = new StringBuffer();

			Map<Object, Object> varMap = null;
			coefficientKeyValue = new Object[listLhs.size()];
			while (iteratorLhs.hasNext()) {
				LHSCoefficient lhsCoefficient = iteratorLhs.next();
				constraint.setCoefficient(lhsCoefficient.getCoefficient());
				Query queryVar = session.createQuery("from Variable where Id = :Id and giaFlag!=:flag");
				queryVar.setParameter("Id", lhsCoefficient.getVariableKey());
				queryVar.setParameter("flag", GiaConstants.NO_APPLICABLE);
				List<Variable> listVar = queryVar.list();

				Iterator<Variable> iteratorvar = listVar.iterator();
				Variable var = null;
				varMap = new HashMap<Object, Object>();
				while (iteratorvar.hasNext()) {
					var = iteratorvar.next();
					sfVar.append(var.getAlias());
					sfVar.append(",");
					constraint.setLHSKey(var.getId());
					varMap.put("variable", var.getAlias());
					varMap.put("coefficient", lhsCoefficient.getCoefficient());
					varMap.put("lhskey", var.getId());
				}
				
				coefficientKeyValue[countVar] = varMap;
				
				countVar++;
				constraint.setVariableName(sfVar.toString());
				constraint.setCoefficientKeyValue(coefficientKeyValue);
			}

			constraint.setRhs(equation.getRhs());
			constraint.setComm(equation.getComm());
			constraint.setOper(equation.getOper());
			list2.add(constraint);

		}
		if (status != null && status.getCount() != null && status.getCount() > 0) {
			Map map = new HashMap();

			String statusMsg = status.getEqn() + " with ID " + status.getId() + " has been created successfully.";
			map.put("statusMsg", statusMsg);
			status.setCount(new Long(0));
			list2.add(map);
		}
		return list2;
	}

	@Transactional
	public void deleteConstraint(ConstraintModel model) {
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("from ConstraintEquation where Id = :Id ");
		query.setParameter("Id", model.getId());
		List list = query.list();
		Iterator<ConstraintEquation> iterator = list.iterator();
		while (iterator.hasNext()) {
			ConstraintEquation constrnt = iterator.next();
			session.delete(constrnt);
		}
		/*
		 * for published CEQuery query = session.
		 * createQuery("update ConstraintEquation set publishStatus = :status" +
		 * " where Id = :Id"); query.setParameter("status", "DEL");
		 * 
		 * query.setParameter("Id", model.getId()); int result =
		 * query.executeUpdate();
		 */
	}

	@Transactional
	@Override
	public void updateConstraint(ConstraintModel constraintModel, String name) throws ParseException {
		Session session = sessionFactory.getCurrentSession();
		Query selectQuery = session.createQuery("select Id from ConstraintEquation " + " where eqn = :eqn");
		selectQuery.setParameter("eqn", constraintModel.getEqn());
		List list = selectQuery.list();
		ConstraintEquation constrnt = new ConstraintEquation();
		Iterator<Long> iterator = list.iterator();
		Long Id = null;
		while (iterator.hasNext()) {
			Id = iterator.next();
		}
		constrnt.setId(Id);

		Query selectQueryConstrntToGrp = session
				.createQuery("from ConstraintToGroup " + " where constraintEq = :constraintEq");
		selectQueryConstrntToGrp.setParameter("constraintEq", constrnt);
		List consGrpList = selectQueryConstrntToGrp.list();
		Iterator<ConstraintToGroup> iteratorList = consGrpList.iterator();
		Long IdGrp = null;
		while (iteratorList.hasNext()) {
			ConstraintToGroup consTGrp = iteratorList.next();
			session.delete(consTGrp);
		}
		Object[] o = constraintModel.getConstraintGrpKey();
		int length = constraintModel.getConstraintGrpKey().length;
		
		for (int i = 0; i < length; i++) {
			ConstraintToGroup constraintToGroup = new ConstraintToGroup();
			Map o1 = (HashMap) o[i];
			int constraintKey = (Integer) o1.get("constraintGroupKey");
			constraintToGroup.setConstraintEq(constrnt);
			constraintToGroup.setModifiedTimeStamp(new Timestamp(System.currentTimeMillis()));
			constraintToGroup.setModifiedBy(name);
			constraintToGroup.setHistoryKey(new Long(1));
			constraintToGroup.setConstraintGroupKey(new Long(constraintKey));
			session.save(constraintToGroup);
		}
		String isEnabled=null;
		if (constraintModel.getIsEnabled() != null && !constraintModel.getIsEnabled().equals("")) {
			if (constraintModel.getIsEnabled().equals("true")) {
				isEnabled="T";
			} else {
				isEnabled="F";
			}
		} else {
			isEnabled="F";
		}

		Criteria crit = session.createCriteria(ConstraintEquation.class);
        crit.add(Restrictions.eq("eqn", constraintModel.getEqn()));
        ScrollableResults items = crit.scroll();
        int count=0;
        while ( items.next() ) {
        	ConstraintEquation equation = (ConstraintEquation)items.get(count);
            equation.setPublishStatus("EDIT");
            equation.setDesc(constraintModel.getDesc());
            equation.setVersion(constraintModel.getVersion()+1);
            equation.setRhs(constraintModel.getRhs());
            equation.setOper(constraintModel.getOper());
            equation.setIsEnabled(isEnabled);
            equation.setStartDate(new Timestamp(constraintModel.getStartDate().getTime()));
            equation.setEndDate(new Timestamp(constraintModel.getEndDate().getTime()));
            session.saveOrUpdate(equation);
            count++;
        }

	}

}
