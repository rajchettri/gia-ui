package au.com.wp.corp.gia.dto;
import com.fasterxml.jackson.annotation.JsonProperty;
public class Group {
	//@JsonProperty("group")
	private String group;
	
	private String groupName;
	
	private String groupDescription;
	
	private Long constraintGroupKey;
	
	private String lastModifiedTimestamp;
	
	private String lastModifiedBy;
	
	private boolean hasEquation;

	
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Long getConstraintGroupKey() {
		return constraintGroupKey;
	}

	public void setConstraintGroupKey(Long constraintGroupKey) {
		this.constraintGroupKey = constraintGroupKey;
	}

	

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getLastModifiedTimestamp() {
		return lastModifiedTimestamp;
	}

	public void setLastModifiedTimestamp(String lastModifiedTimestamp) {
		this.lastModifiedTimestamp = lastModifiedTimestamp;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public boolean isHasEquation() {
		return hasEquation;
	}

	public void setHasEquation(boolean hasEquation) {
		this.hasEquation = hasEquation;
	}

		
}
