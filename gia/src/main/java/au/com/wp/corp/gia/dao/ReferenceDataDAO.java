package au.com.wp.corp.gia.dao;

import java.util.List;

import au.com.wp.corp.gia.domain.ConstraintGroup;
import au.com.wp.corp.gia.domain.Mirror;
import au.com.wp.corp.gia.domain.Variable;
import au.com.wp.corp.gia.dto.Group;
import au.com.wp.corp.gia.dto.PlatformStatus;

public interface ReferenceDataDAO {
	
	public void insertVariableData(Variable variable);
	
	public void deleteVariableData(String variable);
	
	public List<Variable> getVariableDatas();
	
	public void insertGroup(ConstraintGroup group);
	
	public void deleteGroup(String group);
	
	public List<ConstraintGroup> getGroups();
	
	public void insertMirrors(Mirror mirror);
	
	public List<Mirror> getMirrors();

	public void updateVariableData(Variable variable);

	public void updateGroupData(ConstraintGroup groupDomain);

	public void updateMirror(Mirror mirror);

	public Variable getVariableById(String variableKey);

	public ConstraintGroup getGroupDataById(String id);

	public Mirror getMirrorById(String id);
	
	public Boolean isVariableUsedEquation(Long id);
	
	public Boolean isGroupUsedEquation(Long id);
}
