package au.com.wp.corp.gia.dao;


import java.text.ParseException;
import java.util.List;

import au.com.wp.corp.gia.dto.ConstraintModel;

public interface ConstraintDao {
	public void insertConstraint(ConstraintModel model,String name) throws ParseException;
	
	public void updateConstraint(ConstraintModel model,String name) throws ParseException;
	
	public ConstraintModel retrieveConstraint();
	
	public void deleteConstraint(ConstraintModel model);
	
	public List retrieveAllConstraint();
}
