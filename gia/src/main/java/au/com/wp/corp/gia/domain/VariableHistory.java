package au.com.wp.corp.gia.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="VARIABLE_HISTORY")
public class VariableHistory {


	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "VARIABLE_HISTORY_KEY", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	@Column(name = "VARIABLE_HISTORY_KEY", nullable = false)
	private Long Id;
	
	@Column(name = "VARIABLE_KEY", nullable = false)
	private Long variableKey;
	
	@Column(name="NAME", nullable = false)
	private String name;
	
	@Column(name="ALIAS", nullable = false)
	private String alias;
	
	@Column(name="GIA_FLAG", nullable = false)
	private String giaFlag;
	
	@Column(name="LAST_MODIFIED_TIMESTAMP", nullable = false)
	private Timestamp modifiedTimestamp;
	
	@Column(name="LAST_MODIFIED_BY", nullable = false)
	private String modifiedBy;
	
	@Column(name="CURRENT_HISTORY_KEY", nullable = false)
	private Long historyKey;
	
	@Column(name="OBJECTIVE_FUNCTION_COEFFICIENT", nullable = true)
	private Long objectiveFuncCoefficient;
	
	@Column(name="DUMMY_FLAG", nullable = true)
	private String dummyFlag;
	
	@Column(name="SCADA_ACTUAL_PI_TAG", nullable = true)
	private String scadaActualPiTag;
	
	@Column(name="SCADA_ACTUAL_PI_QUALITY_TAG", nullable = true)
	private String scadaActualQualityPiTag;
	
	@Column(name="STATE_ESTIMATED_PI_TAG", nullable = true)
	private String stateEstimatedPiTag;
	
	@Column(name="XA_OVERRIDE_PI_TAG", nullable = true)
	private String XAOverridePiTag;
	
	@Column(name="FACILITY_NAME", nullable = true)
	private String facilityName;
	
	@Column(name="SCADA_FORECAST_PI_TAG", nullable = true)
	private String scadaForecastPiTag;
	
	@Column(name="SCADA_FORECAST_PI_QUALITY_TAG", nullable = true)
	private String scadaForcastPiQualityTag;
	
	@Column(name="DESCRIPTION", nullable=true)
	private String description;
	

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		this.Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getGiaFlag() {
		return giaFlag;
	}

	public void setGiaFlag(String giaFlag) {
		this.giaFlag = giaFlag;
	}

	public Timestamp getModifiedTimestamp() {
		return modifiedTimestamp;
	}

	public void setModifiedTimestamp(Timestamp modifiedTimestamp) {
		this.modifiedTimestamp = modifiedTimestamp;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Long getHistoryKey() {
		return historyKey;
	}

	public void setHistoryKey(Long historyKey) {
		this.historyKey = historyKey;
	}

	public Long getObjectiveFuncCoefficient() {
		return objectiveFuncCoefficient;
	}

	public void setObjectiveFuncCoefficient(Long objectiveFuncCoefficient) {
		this.objectiveFuncCoefficient = objectiveFuncCoefficient;
	}

	public String getDummyFlag() {
		return dummyFlag;
	}

	public void setDummyFlag(String dummyFlag) {
		this.dummyFlag = dummyFlag;
	}

	public String getScadaActualPiTag() {
		return scadaActualPiTag;
	}

	public void setScadaActualPiTag(String scadaActualPiTag) {
		this.scadaActualPiTag = scadaActualPiTag;
	}

	public String getScadaActualQualityPiTag() {
		return scadaActualQualityPiTag;
	}

	public void setScadaActualQualityPiTag(String scadaActualQualityPiTag) {
		this.scadaActualQualityPiTag = scadaActualQualityPiTag;
	}

	public String getStateEstimatedPiTag() {
		return stateEstimatedPiTag;
	}

	public void setStateEstimatedPiTag(String stateEstimatedPiTag) {
		this.stateEstimatedPiTag = stateEstimatedPiTag;
	}

	public String getXAOverridePiTag() {
		return XAOverridePiTag;
	}

	public void setXAOverridePiTag(String xAOverridePiTag) {
		XAOverridePiTag = xAOverridePiTag;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getScadaForecastPiTag() {
		return scadaForecastPiTag;
	}

	public void setScadaForecastPiTag(String scadaForecastPiTag) {
		this.scadaForecastPiTag = scadaForecastPiTag;
	}

	public String getScadaForcastPiQualityTag() {
		return scadaForcastPiQualityTag;
	}

	public void setScadaForcastPiQualityTag(String scadaForcastPiQualityTag) {
		this.scadaForcastPiQualityTag = scadaForcastPiQualityTag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getVariableKey() {
		return variableKey;
	}

	public void setVariableKey(Long variableKey) {
		this.variableKey = variableKey;
	}


}
