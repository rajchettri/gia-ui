package au.com.wp.corp.gia.domain;


import java.sql.Timestamp;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import static au.com.wp.corp.gia.domain.DbConstants.C_EQUATION_KEY;

import javax.persistence.*;
@Entity
@Table(name = "CONSTRAINT_TO_GROUP")
public  class ConstraintToGroup {
	@Id
	  @GeneratedValue(generator = "CONSTRAINT_TO_GROUP_KEY")
	  @GenericGenerator(strategy = "sequence", name = "CONSTRAINT_TO_GROUP_KEY", parameters = { @Parameter(name = "sequence", value =   "CONSTRAINT_TO_GROUP_KEY") })
	  @Column(name = "CONSTRAINT_TO_GROUP_KEY", nullable = false)
	  private Long constraintToGroupKey;
	
	//	@ManyToOne
	//@JoinColumn(name = "CONSTRAINT_GROUP_KEY")
	//private ConstraintGroup constraintGroup;
	@Column(name = "CONSTRAINT_GROUP_KEY")
	private Long constraintGroupKey;
	
	@ManyToOne
	@JoinColumn(name = "CONSTRAINT_EQUATION_KEY")
	private ConstraintEquation constraintEq;

	
	 @Column(name="LAST_MODIFIED_TIMESTAMP", nullable = false)
	 private Timestamp modifiedTimeStamp;
	 
	 
	 @Column(name="LAST_MODIFIED_BY", nullable = false)
	 private String modifiedBy;
	 
	 @Column(
		        name="CURRENT_HISTORY_KEY",
		        nullable=false,
		        unique=true,
		        columnDefinition = "NUMBER DEFAULT nextval('CONSTRAINT_TO_GROUP_HIST_KEY')"
			 )
	 @Generated(GenerationTime.INSERT)
	 private Long historyKey;


	public Timestamp getModifiedTimeStamp() {
		return modifiedTimeStamp;
	}

	public void setModifiedTimeStamp(Timestamp modifiedTimeStamp) {
		this.modifiedTimeStamp = modifiedTimeStamp;
	}
	
	
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	
	public Long getHistoryKey() {
		return historyKey;
	}

	public void setHistoryKey(Long historyKey) {
		this.historyKey = historyKey;
	}

	

	public ConstraintEquation getConstraintEq() {
		return constraintEq;
	}

	public void setConstraintEq(ConstraintEquation constraintEq) {
		this.constraintEq = constraintEq;
	}

	public Long getConstraintGroupKey() {
		return constraintGroupKey;
	}

	public void setConstraintGroupKey(Long constraintGroupKey) {
		this.constraintGroupKey = constraintGroupKey;
	}

	
	
}

