package au.com.wp.corp.gia.domain;
import static javax.persistence.InheritanceType.JOINED;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.DiscriminatorType.STRING;
import static au.com.wp.corp.gia.domain.DbConstants.SCHEMA;
import static au.com.wp.corp.gia.domain.DbConstants.C_EQUATION_KEY;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SelectBeforeUpdate;

import static au.com.wp.corp.gia.domain.DbConstants.C_EQUATION_KEY;

import javax.persistence.*;
@Entity
@Table(name = "CONSTRAINT_GROUP")
@DynamicUpdate(value=true)
@SelectBeforeUpdate(value=true)
public  class ConstraintGroup {
	@Id
	  @GeneratedValue(generator = "CONSTRAINT_GROUP_KEY")
	  @GenericGenerator(strategy = "sequence", name = "CONSTRAINT_GROUP_KEY", parameters = { @Parameter(name = "sequence", value =   "CONSTRAINT_GROUP_KEY") })
	  @Column(name = "CONSTRAINT_GROUP_KEY", nullable = false)
	  private Long Id;
	

	 @Column(name="GROUP_NAME", nullable = false)
	 private String group;
	 @Column(name="LAST_MODIFIED_TIMESTAMP", nullable = false)
	 private Timestamp modifiedTimeStamp;
	 @Column(name="GROUP_DESCRIPTION", nullable = false)
	 private String groupDescription;
	 
	 @Column(name="LAST_MODIFIED_BY", nullable = false)
	 private String modifiedBy;
	 
	 @Column(
		        name="CURRENT_HISTORY_KEY",
		        nullable=false,
		        unique=true,
		        columnDefinition = "NUMBER DEFAULT nextval('CONSTRAINT_GROUP_HISTORY_KEY')"
			 )
	 @Generated(GenerationTime.INSERT)
	 private Long historyKey;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "constraintEq")
	private Set<ConstraintToGroup> constraintToGroupList;
	

	
	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Timestamp getModifiedTimeStamp() {
		return modifiedTimeStamp;
	}

	public void setModifiedTimeStamp(Timestamp modifiedTimeStamp) {
		this.modifiedTimeStamp = modifiedTimeStamp;
	}
	
	
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	
	public Long getHistoryKey() {
		return historyKey;
	}

	public void setHistoryKey(Long historyKey) {
		this.historyKey = historyKey;
	}

	public Set<ConstraintToGroup> getConstraintToGroupList() {
		return constraintToGroupList;
	}

	public void setConstraintToGroupList(Set<ConstraintToGroup> constraintToGroupList) {
		this.constraintToGroupList = constraintToGroupList;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	
	 
	 
}

