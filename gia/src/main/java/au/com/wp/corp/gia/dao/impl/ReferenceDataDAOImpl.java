package au.com.wp.corp.gia.dao.impl;

import java.sql.JDBCType;
import java.util.List;

import oracle.jdbc.oracore.OracleType;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import au.com.wp.corp.gia.dao.ReferenceDataDAO;
import au.com.wp.corp.gia.domain.ConstraintGroup;
import au.com.wp.corp.gia.domain.Mirror;
import au.com.wp.corp.gia.domain.Variable;
import au.com.wp.corp.gia.dto.Group;
import au.com.wp.corp.gia.dto.PlatformStatus;
import au.com.wp.corp.gia.dto.VariableData;

@Component
@Transactional
public class ReferenceDataDAOImpl implements ReferenceDataDAO {
	
	private static Logger log = LoggerFactory.getLogger(ReferenceDataDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Variable> getVariableDatas() {
		
		Session session = sessionFactory.getCurrentSession();
		List<Variable> list = session.createQuery("from Variable").list();
		return list;
	}

	@Override
	public void insertMirrors(Mirror mirror) {
		Session session = sessionFactory.getCurrentSession();
		session.save(mirror);
	}

	@Override
	public List<ConstraintGroup> getGroups() {
		Session session = sessionFactory.getCurrentSession();
		List<ConstraintGroup> list = session.createQuery("from ConstraintGroup").list();
		return list;
	}

	@Override
	public void insertVariableData(Variable variable) {
		Session session = sessionFactory.getCurrentSession();
		session.save(variable);
	}

	@Override
	public void deleteVariableData(String variable) {
		log.info("In DAO : delete variable id: {}",variable);
		Session session = sessionFactory.getCurrentSession();
		Variable result = (Variable)session.load(Variable.class,Long.valueOf(variable));
	    session.delete(result);
	    //This makes the pending delete to be done
	    session.flush() ;
	    log.info("In DAO : Variable Deleted");
	}

	@Override
	public void insertGroup(ConstraintGroup group) {
		Session session = sessionFactory.getCurrentSession();
		session.save(group);
	}

	@Override
	public void deleteGroup(String group) {
		Session session = sessionFactory.getCurrentSession();
		ConstraintGroup result = (ConstraintGroup)session.load(ConstraintGroup.class,Long.valueOf(group));
	    session.delete(result);
	    //This makes the pending delete to be done
	    session.flush() ;
	}

	@Override
	public List<Mirror> getMirrors() {
		Session session = sessionFactory.getCurrentSession();
		List<Mirror> list = session.createQuery("from Mirror").list();
		return list;
	}

	@Override
	public void updateVariableData(Variable variable) {
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("select GIA.VARIABLE_HISTORY_KEY.nextval as num from dual")
		            .addScalar("num", StandardBasicTypes.LONG);
		
		variable.setHistoryKey(((Long) query.uniqueResult()).longValue());
		//Update query
		session.update(variable);
	}

	@Override
	public void updateGroupData(ConstraintGroup groupDomain) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("select GIA.CONSTRAINT_GROUP_HISTORY_KEY.nextval as num from dual")
	            .addScalar("num", StandardBasicTypes.LONG);
	
		groupDomain.setHistoryKey(((Long) query.uniqueResult()).longValue());
		//Update query
		session.update(groupDomain);
	}

	@Override
	public void updateMirror(Mirror mirror) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("select GIA.MIRROR_HISTORY_KEY.nextval as num from dual")
	            .addScalar("num", StandardBasicTypes.LONG);
	
		mirror.setCurrentHistoryKey(((Long) query.uniqueResult()).longValue());
		//Update query
		session.update(mirror);
	}

	@Override
	public Variable getVariableById(String variableKey) {
		Variable variable = null;
		Session session = sessionFactory.getCurrentSession();
		//Update query
		//session.save(mirror);
		long longId = Long.parseLong(variableKey);
		variable = (Variable) session.get(Variable.class, longId);
		return variable;
	}

	@Override
	public ConstraintGroup getGroupDataById(String id) {
		ConstraintGroup group = null;
		Session session = sessionFactory.getCurrentSession();
		//Update query
		//session.save(mirror);
		long longId = Long.parseLong(id);
		group = (ConstraintGroup) session.get(ConstraintGroup.class, longId);
		return group;
	}

	@Override
	public Mirror getMirrorById(String id) {
		Mirror mirror = null;
		Session session = sessionFactory.getCurrentSession();
		//Update query
		//session.save(mirror);
		long longId = Long.parseLong(id);
		mirror =  (Mirror) session.get(Mirror.class, longId);
		return mirror;
	}

	@Override
	public Boolean isVariableUsedEquation(Long id) {

		boolean inUse = false;
		Session session = sessionFactory.getCurrentSession();
		Long variableKey = (Long) session.createQuery("select COUNT(variableKey) from LHSCoefficient where variableKey = :KEY")
					.setLong("KEY",id)
					.uniqueResult();
		
		if(variableKey != null && variableKey > 0){
			inUse = true;
		}
		return inUse;
	}
	
	@Override
	public Boolean isGroupUsedEquation(Long id) {
		
		boolean hasEquation = false;
		Session session = sessionFactory.getCurrentSession();
		Long groupKey = (Long) session.createQuery("SELECT COUNT(*) FROM ConstraintToGroup WHERE constraintGroupKey=:KEY")
					.setLong("KEY",id)
					.uniqueResult();
		
		if(groupKey != null && groupKey > 0){
			hasEquation = true;
		}
		return hasEquation;
		
	}
}
