package au.com.wp.corp.gia.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name="MIRROR")
public class Mirror {

	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "MIRROR_KEY", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	@Column(name = "MIRROR_KEY", nullable = false)
	private Long id;
	
	@Column(name = "MIRROR_STATUS", nullable = false)
	private String mirrorStatus;
	
	@Column(name = "OWNER", nullable = false)
	private String owner;
	
	@Column(name = "MIRROR_DESCRIPTION", nullable = false)
	private String mirrorDescription;
	
	@Column(name = "LAST_MODIFIED_TIMESTAMP", nullable = false)
	private Timestamp lastModifiedTimestamp;
	
	@Column(name = "LAST_MODIFIED_BY", nullable = false)
	private String lastModifiedBy;
	
	@Column(
	        name="CURRENT_HISTORY_KEY",
	        nullable=false,
	        unique=true,
	        columnDefinition = "NUMBER DEFAULT nextval('MIRROR_HISTORY_KEY')"
	)
	@Generated(GenerationTime.INSERT)
	private Long currentHistoryKey;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMirrorStatus() {
		return mirrorStatus;
	}

	public void setMirrorStatus(String mirrorStatus) {
		this.mirrorStatus = mirrorStatus;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getMirrorDescription() {
		return mirrorDescription;
	}

	public void setMirrorDescription(String mirrorDescription) {
		this.mirrorDescription = mirrorDescription;
	}

	public Timestamp getLastModifiedTimestamp() {
		return lastModifiedTimestamp;
	}

	public void setLastModifiedTimestamp(Timestamp lastModifiedTimestamp) {
		this.lastModifiedTimestamp = lastModifiedTimestamp;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Long getCurrentHistoryKey() {
		return currentHistoryKey;
	}

	public void setCurrentHistoryKey(Long currentHistoryKey) {
		this.currentHistoryKey = currentHistoryKey;
	}
	
	
}
