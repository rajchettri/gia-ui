package au.com.wp.corp.gia.dto;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VariableModel {
	@JsonProperty("variable")
	private String variable;
	
	//@JsonProperty("lhskey")
	private Long lhskey;

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public Long getLhskey() {
		
		return lhskey;
	}

	public void setLhskey(Long lhskey) {
		this.lhskey = lhskey;
	}

	
	
	
}
