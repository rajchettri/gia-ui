package au.com.wp.corp.gia.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.com.wp.corp.gia.business.GIAReferenceDataHelper;
import au.com.wp.corp.gia.dto.Group;
import au.com.wp.corp.gia.dto.PlatformStatus;
import au.com.wp.corp.gia.dto.ResponseStatus;
import au.com.wp.corp.gia.dto.VariableData;


/**
 * @author n039804
 *
 */
@RestController
@RequestMapping("/referencedata")
public class ReferenceDataController {

	private static Logger logger = LoggerFactory.getLogger(ReferenceDataController.class);
	@Autowired
	private GIAReferenceDataHelper referenceDataHelper;
	
	@RequestMapping(value="/variable", method=RequestMethod.POST)
	public ResponseEntity<VariableData> saveVariableData(@RequestBody VariableData variableData) {
		logger.info("Insert Variable Data :----");
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		variableData.setLastModifiedBy(auth.getName());
		VariableData data = referenceDataHelper.insertVariableData(variableData);
		return new ResponseEntity<VariableData>(data, HttpStatus.OK);
	}
	
	@RequestMapping(value="/variable/{variableKey}", method=RequestMethod.DELETE)
	public ResponseEntity<ResponseStatus> deleteVariableData( @PathVariable("variableKey") String variableKey) {
		
		ResponseStatus responseStatus = new ResponseStatus();
		try{
			referenceDataHelper.deleteVariableData(variableKey);
			
			responseStatus.setMsg("success");
			responseStatus.setStatus("OK");
		}catch(Exception e){
			responseStatus.setMsg(e.getMessage());
			responseStatus.setStatus("NOT OK");
		}
		
		return new ResponseEntity<ResponseStatus>(responseStatus, HttpStatus.OK);
	}
	
	@RequestMapping(value="/variable/{variableKey}", method=RequestMethod.POST)
	public ResponseEntity<VariableData> updateVariableData(@RequestBody VariableData variableData, @PathVariable("variableKey") String variableKey) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		variableData.setLastModifiedBy(auth.getName());
		VariableData result= referenceDataHelper.updateVariableData(variableData,variableKey);
		return new ResponseEntity<VariableData>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/group", method=RequestMethod.POST)
	public ResponseEntity<Group> saveGroupsData(@RequestBody Group group) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		group.setLastModifiedBy(auth.getName());
		Group data = referenceDataHelper.insertGroupData(group);
		return new ResponseEntity<Group>(data, HttpStatus.OK);
	}
	
	@RequestMapping(value="/group/{constraintGroupKey}", method=RequestMethod.DELETE)
	public ResponseEntity<ResponseStatus> deleteGroupsData(@PathVariable("constraintGroupKey") String constraintGroupKey) {
		
		ResponseStatus responseStatus = new ResponseStatus();
		try{
			referenceDataHelper.deleteGroupData(constraintGroupKey);
			
			responseStatus.setMsg("success");
			responseStatus.setStatus("OK");
		}catch(Exception e){
			responseStatus.setMsg(e.getMessage());
			responseStatus.setStatus("NOT OK");
		}
		
		return new ResponseEntity<ResponseStatus>(responseStatus, HttpStatus.OK);
	}
	@RequestMapping(value="/platform", method=RequestMethod.POST)
	public ResponseEntity<PlatformStatus> savePlatformStatus(@RequestBody PlatformStatus platformStatus) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		platformStatus.setLastModifiedBy(auth.getName());
		PlatformStatus data = referenceDataHelper.insertPlatform(platformStatus);
		return new ResponseEntity<PlatformStatus>(data, HttpStatus.OK);
	}
	
	@RequestMapping(value="/variable", method=RequestMethod.GET)
	public ResponseEntity<List<VariableData>> getVariableData(){
		
		List<VariableData> variableDatas = referenceDataHelper.getVariableDatas();
		
		return new ResponseEntity<List<VariableData>>(variableDatas, HttpStatus.OK);
	}
	
	@RequestMapping(value="/variable/{variable_key}", method=RequestMethod.GET)
	public ResponseEntity<VariableData> getVariableDataById(@PathVariable("variable_key") String variableKey){
		
		//List<VariableData> variableDatas = referenceDataHelper.getVariableDatas();
		VariableData variable = referenceDataHelper.getVariableDataById(variableKey);
		
		return new ResponseEntity<VariableData>(variable, HttpStatus.OK);
	}
	
	@RequestMapping(value="/group", method=RequestMethod.GET)
	public ResponseEntity<List<Group>> getGroups(){
		
		List<Group> groups = referenceDataHelper.getGroupDatas();
		
		return new ResponseEntity<List<Group>>(groups, HttpStatus.OK);
	}
	
	@RequestMapping(value="/group/{id}", method=RequestMethod.GET)
	public ResponseEntity<Group> getGroupsbyId(@PathVariable("id") String id){
		
		Group group = referenceDataHelper.getGroupDataById(id);
		
		return new ResponseEntity<Group>(group, HttpStatus.OK);
	}
	
	@RequestMapping(value="/platform", method=RequestMethod.GET)
	public ResponseEntity<List<PlatformStatus>> getPlatforms(){
		
		List<PlatformStatus> platforms = referenceDataHelper.getPlatforms();
		
		return new ResponseEntity<List<PlatformStatus>>(platforms, HttpStatus.OK);
	}
	
	@RequestMapping(value="/platform/{mirror_key}", method=RequestMethod.GET)
	public ResponseEntity<PlatformStatus> getPlatformById(@PathVariable("mirror_key") String mirror_key){
		
		PlatformStatus platform = referenceDataHelper.getPlatformById(mirror_key);
		
		return new ResponseEntity<PlatformStatus>(platform, HttpStatus.OK);
	}
	
	@RequestMapping(value="/group/{constraintGroupKey}", method=RequestMethod.POST)
	public ResponseEntity<Group> updateGroupData(@RequestBody Group group, @PathVariable("constraintGroupKey") String constraintGroupKey) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		group.setLastModifiedBy(auth.getName());
		Group result = referenceDataHelper.updateGroupData(group,constraintGroupKey);
		return new ResponseEntity<Group>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/platform/{mirrorKey}", method=RequestMethod.POST)
	public ResponseEntity<PlatformStatus> updatePlatformData(@RequestBody PlatformStatus platform, @PathVariable("mirrorKey") String mirrorKey) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		platform.setLastModifiedBy(auth.getName());
		PlatformStatus result = referenceDataHelper.updatePlatformData(platform,mirrorKey);
		return new ResponseEntity<PlatformStatus>(result, HttpStatus.OK);
	}
	
}
