package au.com.wp.corp.gia.dto;

public class PlatformStatus {
	
	private Long mirrorKey;
	
	private String mirrorStatus;
	
	private String owner;
	
	private String mirrorDescription;
	
	private String lastModifiedTimestamp;
	
	private String lastModifiedBy;
	
	//private Long currentHistoryKey;

	

	

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	

	public String getLastModifiedTimestamp() {
		return lastModifiedTimestamp;
	}

	public void setLastModifiedTimestamp(String lastModifiedTimestamp) {
		this.lastModifiedTimestamp = lastModifiedTimestamp;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Long getMirrorKey() {
		return mirrorKey;
	}

	public void setMirrorKey(Long mirrorKey) {
		this.mirrorKey = mirrorKey;
	}

	public String getMirrorStatus() {
		return mirrorStatus;
	}

	public void setMirrorStatus(String mirrorStatus) {
		this.mirrorStatus = mirrorStatus;
	}

	public String getMirrorDescription() {
		return mirrorDescription;
	}

	public void setMirrorDescription(String mirrorDescription) {
		this.mirrorDescription = mirrorDescription;
	}

	/*public Long getCurrentHistoryKey() {
		return currentHistoryKey;
	}

	public void setCurrentHistoryKey(Long currentHistoryKey) {
		this.currentHistoryKey = currentHistoryKey;
	}
	*/

}
