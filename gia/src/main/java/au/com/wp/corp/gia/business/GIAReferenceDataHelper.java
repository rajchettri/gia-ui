package au.com.wp.corp.gia.business;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.wp.corp.gia.dao.ReferenceDataDAO;
import au.com.wp.corp.gia.domain.ConstraintGroup;
import au.com.wp.corp.gia.domain.Mirror;
import au.com.wp.corp.gia.domain.Variable;
import au.com.wp.corp.gia.dto.Group;
import au.com.wp.corp.gia.dto.PlatformStatus;
import au.com.wp.corp.gia.dto.VariableData;
import au.com.wp.corp.gia.util.GiaConstants;

@Service
public class GIAReferenceDataHelper {
	
	private static Logger log = LoggerFactory.getLogger(GIAReferenceDataHelper.class);

	@Autowired
	private ReferenceDataDAO referenceDataDAO;

	@Autowired
	private DozerBeanMapper mapper;

	public VariableData insertVariableData(VariableData variable) {
		log.info("Service : Process variable data--");
		Timestamp time = new Timestamp(System.currentTimeMillis());
		// Mapping from DTO to domain object using dozer mapping
		Variable domain = mapper.map(variable, Variable.class,"variable");
		domain.setModifiedTimestamp(time);
		if(domain.getDummyFlag() == null) {
			domain.setDummyFlag("F");
		}
		
		// Save the Variable data to GIA DB
		referenceDataDAO.insertVariableData(domain);
		
		VariableData result = getVariableDataById(String.valueOf(domain.getId()));

		return result;
	}

	public void deleteVariableData(String variableKey) {

		log.info("Service : variable key : "+variableKey);
		// Mapping from DTO to domain object using dozer mapping
		//Variable domain = mapper.map(variable, Variable.class,"variable");

		// Save the Variable data to GIA DB
		referenceDataDAO.deleteVariableData(variableKey);

	}

	public List<VariableData> getVariableDatas(){
		
		List<VariableData> resultDatas = new ArrayList<VariableData>();
		
		List<Variable> variables = referenceDataDAO.getVariableDatas();
		VariableData resultVar = null;

		for (Variable var : variables) {
			if (var != null) {
				// Mapping from Domain to DTO object using dozer mapping
				resultVar = mapper.map(var, VariableData.class,"variable");
				resultDatas.add(resultVar);
			}
		}
		return resultDatas;
	}
	
	public Group insertGroupData(Group group) {

		Timestamp time = new Timestamp(System.currentTimeMillis());
		// Mapping from DTO to domain object using dozer mapping
		ConstraintGroup domain = mapper.map(group, ConstraintGroup.class,"group");
		domain.setModifiedTimeStamp(time);
		// Save the Variable data to GIA DB
		referenceDataDAO.insertGroup(domain);

		Group groupRes = getGroupDataById(String.valueOf(domain.getId()));
		return groupRes;
	}

	public void deleteGroupData(String group) {

		// Mapping from DTO to domain object using dozer mapping
		//ConstraintGroup domain = mapper.map(group, ConstraintGroup.class,"group");

		// Save the Variable data to GIA DB
		referenceDataDAO.deleteGroup(group);

	}

	public List<Group> getGroupDatas(){
		
		List<Group> resultDatas = new ArrayList<Group>();
		
		List<ConstraintGroup> groups = referenceDataDAO.getGroups();
		Group resultGroup = null;

		for (ConstraintGroup var : groups) {
			if (var != null) {
				// Mapping from Domain to DTO object using dozer mapping
				resultGroup = mapper.map(var, Group.class,"group");
				resultDatas.add(resultGroup);
			}
		}
		return resultDatas;
	}


	public PlatformStatus insertPlatform(PlatformStatus platform) {

		// Mapping from DTO to domain object using dozer mapping
		Mirror domain = mapper.map(platform, Mirror.class,"platform");
		domain.setMirrorStatus(getMirrorStatusKey(domain.getMirrorStatus()));
		domain.setLastModifiedTimestamp(new Timestamp(System.currentTimeMillis()));
		// Save the Variable data to GIA DB
		referenceDataDAO.insertMirrors(domain);

		PlatformStatus platformRes = getPlatformById(String.valueOf(domain.getId()));
		
		return platformRes;
	}
	
	public List<PlatformStatus> getPlatforms() {
		List<PlatformStatus> resultDatas = new ArrayList<PlatformStatus>();
		
		List<Mirror> mirrors = referenceDataDAO.getMirrors();
		PlatformStatus resultGroup = null;

		for (Mirror var : mirrors) {
			if (var != null) {
				// Mapping from Domain to DTO object using dozer mapping
				resultGroup = mapper.map(var, PlatformStatus.class,"platform");
				resultGroup.setMirrorStatus(getMirrorStatusValue(resultGroup.getMirrorKey(), resultGroup.getMirrorStatus()));
				resultDatas.add(resultGroup);
			}
		}
		return resultDatas;
	}

	public VariableData updateVariableData(VariableData variableData, String id) {
		Variable variable = null;
		variable = mapper.map(variableData, Variable.class, "variable");
		variable.setId(Long.valueOf(id));
		referenceDataDAO.updateVariableData(variable);
		
		return getVariableDataById(id);
	}

	public Group updateGroupData(Group group, String id) {
		ConstraintGroup groupDomain = null;
		groupDomain = mapper.map(group, ConstraintGroup.class, "group");
		groupDomain.setId(Long.valueOf(id));
		referenceDataDAO.updateGroupData(groupDomain);
		
		return getGroupDataById(id);
		
	}

	public PlatformStatus updatePlatformData(PlatformStatus platform, String id) {
		
		Mirror mirror = null;
		mirror = mapper.map(platform, Mirror.class, "platform");
		mirror.setId(Long.valueOf(id));
		String mirrorStatusValue = mirror.getMirrorStatus();
		mirror.setMirrorStatus(getMirrorStatusKey(mirrorStatusValue));
		referenceDataDAO.updateMirror(mirror);
		
		return getPlatformById(id);
	}

	public VariableData getVariableDataById(String variableKey) {
		
		VariableData result= null;
		
		Variable variable= referenceDataDAO.getVariableById(variableKey);
		if(variable != null){
			result = mapper.map(variable, VariableData.class, "variable");
		}
		result.setInUse(isVariableUsedEquation(variable.getId()));
		
		return result;
	}

	public Group getGroupDataById(String id) {
		Group result= null;
		
		ConstraintGroup group= referenceDataDAO.getGroupDataById(id);
		if(group != null){
			result = mapper.map(group, Group.class, "group");
		}
		result.setHasEquation(isGroupUsedEquation(group.getId()));
		return result;
	}

	public PlatformStatus getPlatformById(String id) {
		PlatformStatus result= null;
		
		Mirror mirror= referenceDataDAO.getMirrorById(id);
		if(mirror!= null){
			result = mapper.map(mirror, PlatformStatus.class, "platform");
			result.setMirrorStatus(getMirrorStatusValue(result.getMirrorKey(), result.getMirrorStatus()));
		}
		return result;
	}
	
	
	public String getMirrorStatusValue(Long mirrorKey, String  mirrorStatus){
		
		String status = null;
		if(new Long(0).equals(mirrorKey)){
				status = GiaConstants.ENABLED;
		} else {
			if(GiaConstants.KEY_S.equals(mirrorStatus)){
				status = GiaConstants.SIMULATED;
			} else if(GiaConstants.KEY_D.equals(mirrorStatus)){
				status = GiaConstants.DISABLED;
			}
		}
		
		return status;
	}
	
	public String getMirrorStatusKey( String  mirrorStatus){
		
		String status = null;
			if(GiaConstants.SIMULATED.equals(mirrorStatus)){
				status = GiaConstants.KEY_S;
			} else if(GiaConstants.DISABLED.equals(mirrorStatus)){
				status = GiaConstants.KEY_D;
			} else if(GiaConstants.ENABLED.equals(mirrorStatus)){
				status = GiaConstants.KEY_E;
			}
		return status;
	}
	
	public Boolean isVariableUsedEquation(Long id){
		
		return referenceDataDAO.isVariableUsedEquation(id);
	}
	
	public Boolean isGroupUsedEquation(Long id){
		
		return referenceDataDAO.isGroupUsedEquation(id);
		
	}
}
