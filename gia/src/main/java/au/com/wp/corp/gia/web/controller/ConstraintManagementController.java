package au.com.wp.corp.gia.web.controller;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.com.wp.corp.gia.dao.ConstraintDao;
import au.com.wp.corp.gia.dto.ConstraintModel;
import au.com.wp.corp.gia.dto.Group;

@RestController
public class ConstraintManagementController {

	private static Logger logger = LoggerFactory.getLogger(ConstraintManagementController.class);
	@Autowired
	ConstraintDao constrainDao;

	@RequestMapping(value = "/constraint", produces = { "application/json",
			"application/xml" }, consumes = { "application/x-www-form-urlencoded" }, method = {
			RequestMethod.GET, RequestMethod.POST })
	public String loadConstraint(@RequestParam Map<String, String> body,
			HttpServletRequest req) {
		ModelMap model1 = new ModelMap();

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Collection authorities = auth.getAuthorities();
		Iterator iterator = authorities.iterator();
		String role = null;
		String path = null;
		for (String key : body.keySet()) {
			path = body.get(key);
		}

		model1.addAttribute("userName", auth.getName());
		while (iterator.hasNext()) {
			role = iterator.next().toString();

		}
		if (role.equals("ROLE_USER")) {
			req.setAttribute("Operational", "Operational");
		}
		if (path != null && path.equalsIgnoreCase("Operational")) {
			req.setAttribute("userRole", "Oper");
			return "operational";
		}
		if (path != null && path.equalsIgnoreCase("Testing")) {
			req.setAttribute("userRole", "Test");
			return "constraint";
		}
		return "constraint";

	}

	@RequestMapping(value = "/load", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> loadPage(Group group, HttpServletRequest request) {

		ConstraintModel constraint = constrainDao.retrieveConstraint();
		List list = constrainDao.retrieveAllConstraint();
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		hmap.put("constraint", constraint);
		hmap.put("list", list);

		return new ResponseEntity<HashMap<String, Object>>(hmap, HttpStatus.OK);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<?> saveConstraintEquation(
			@RequestBody ConstraintModel constraint, HttpServletRequest request)
			throws ParseException {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName();
		List list = null;
		Map map = null;
		boolean b = false;
		//Start and End date format change
		if(constraint.getStartDate1() != null&& constraint.getEndDate1()!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			
			Date startDate = sdf.parse(constraint.getStartDate1());
			Date endDate = sdf.parse(constraint.getEndDate1());
			constraint.setStartDate(startDate);
			constraint.setEndDate(endDate);
		}
		//code end
		try {
			if (constraint.getVersion() != null)
				constrainDao.updateConstraint(constraint, name);
			else
				constrainDao.insertConstraint(constraint, name);
		} catch (Exception e) {
			b = true;
			map = new HashMap();
			map.put("isCePresent", "T");
			logger.error("Error occured during insert/update : ",e);
		}

		try {
			list = constrainDao.retrieveAllConstraint();
		} catch (Exception e) {
			logger.error("Error occured during retrieval : ",e);
		}
		if (!b)
			return new ResponseEntity<List>(list, HttpStatus.OK);
		else
			return new ResponseEntity<Map>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<List<ConstraintModel>> deleteConstraintEquation(
			@RequestBody String Id, HttpServletRequest request)
			throws ParseException {

		ConstraintModel constraint = new ConstraintModel(Id);

		constrainDao.deleteConstraint(constraint);

		List<ConstraintModel> list = constrainDao.retrieveAllConstraint();

		return new ResponseEntity<List<ConstraintModel>>(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/operational", method = RequestMethod.GET)
	public String operational(ModelMap model1) {
		return "operational";
	}
	
	@RequestMapping(value="/publish", method = RequestMethod.POST)
	public ResponseEntity<List<ConstraintModel>> publicConstraintEquation(){
		
		return null;
	}
}