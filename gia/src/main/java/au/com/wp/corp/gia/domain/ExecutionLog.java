package au.com.wp.corp.gia.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="EXECUTION_LOG")
public class ExecutionLog {
	
	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "EXECUTION_LOG_KEY", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	@Column(name = "EXECUTION_LOG_KEY", nullable = false)
	private Long id;
	
	@Column(name = "MODEL_EXECUTION_KEY", nullable = false)
	private Long modelExecutionKey;
	
	@Column(name = "EXECUTION_LOG", nullable = true)
	private String executionLog;
	
	@Column(name = "LOG_TIMESTAMP", nullable = false)
	private Timestamp logTimestamp;
	
	@Column(name = "ERROR_LEVEL", nullable = false)
	private String errorLevel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getModelExecutionKey() {
		return modelExecutionKey;
	}

	public void setModelExecutionKey(Long modelExecutionKey) {
		this.modelExecutionKey = modelExecutionKey;
	}

	public String getExecutionLog() {
		return executionLog;
	}

	public void setExecutionLog(String executionLog) {
		this.executionLog = executionLog;
	}

	public Timestamp getLogTimestamp() {
		return logTimestamp;
	}

	public void setLogTimestamp(Timestamp logTimestamp) {
		this.logTimestamp = logTimestamp;
	}

	public String getErrorLevel() {
		return errorLevel;
	}

	public void setErrorLevel(String errorLevel) {
		this.errorLevel = errorLevel;
	}

}
