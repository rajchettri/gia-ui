package au.com.wp.corp.gia.dto;

public class VariableData {
	
	private Long variableKey;
	private String name;
	private String alias;
	private String description;
	private String giaFlag;
	private String facilityName;
	private String scadaActualPiTag;
	private String scadaActualPiQualityTag;
	private String stateEstimatedPiTag;
	private String xaOverridePiTag;
	private String scadaForecastPiTag;
	private String scadaForecastPiQualityTag;
	private Long objectiveFunctionCoefficient;
	private String lastModifiedBy;
	private String lastModifiedTimestamp;
	//private Long currentHistoryKey;
	private String dummyFlag;
	private Boolean inUse;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getFacilityName() {
		return facilityName;
	}
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	public String getScadaActualPiTag() {
		return scadaActualPiTag;
	}
	public void setScadaActualPiTag(String scadaActualPiTag) {
		this.scadaActualPiTag = scadaActualPiTag;
	}
	
	public String getStateEstimatedPiTag() {
		return stateEstimatedPiTag;
	}
	public void setStateEstimatedPiTag(String stateEstimatedPiTag) {
		this.stateEstimatedPiTag = stateEstimatedPiTag;
	}
	
	public String getXaOverridePiTag() {
		return xaOverridePiTag;
	}
	public void setXaOverridePiTag(String overridePiTag) {
		this.xaOverridePiTag = overridePiTag;
	}
	public Long getVariableKey() {
		return variableKey;
	}
	public void setVariableKey(Long variableKey) {
		this.variableKey = variableKey;
	}
	public String getGiaFlag() {
		return giaFlag;
	}
	public void setGiaFlag(String giaFlag) {
		this.giaFlag = giaFlag;
	}
	public String getScadaActualPiQualityTag() {
		return scadaActualPiQualityTag;
	}
	public void setScadaActualPiQualityTag(String scadaActualPiQualityTag) {
		this.scadaActualPiQualityTag = scadaActualPiQualityTag;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public String getLastModifiedTimestamp() {
		return lastModifiedTimestamp;
	}
	public void setLastModifiedTimestamp(String lastModifiedTimestamp) {
		this.lastModifiedTimestamp = lastModifiedTimestamp;
	}
	public String getScadaForecastPiTag() {
		return scadaForecastPiTag;
	}
	public void setScadaForecastPiTag(String scadaForecastPiTag) {
		this.scadaForecastPiTag = scadaForecastPiTag;
	}
	public String getScadaForecastPiQualityTag() {
		return scadaForecastPiQualityTag;
	}
	public void setScadaForecastPiQualityTag(String scadaForecastPiQualityTag) {
		this.scadaForecastPiQualityTag = scadaForecastPiQualityTag;
	}
	/*public Long getCurrentHistoryKey() {
		return currentHistoryKey;
	}
	public void setCurrentHistoryKey(Long currentHistoryKey) {
		this.currentHistoryKey = currentHistoryKey;
	}*/
	public Long getObjectiveFunctionCoefficient() {
		return objectiveFunctionCoefficient;
	}
	public void setObjectiveFunctionCoefficient(
			Long objectiveFunctionCoefficient) {
		this.objectiveFunctionCoefficient = objectiveFunctionCoefficient;
	}
	public String getDummyFlag() {
		return dummyFlag;
	}
	public void setDummyFlag(String dummyFlag) {
		this.dummyFlag = dummyFlag;
	}
	public Boolean getInUse() {
		return inUse;
	}
	public void setInUse(Boolean inUse) {
		this.inUse = inUse;
	}
	
}
