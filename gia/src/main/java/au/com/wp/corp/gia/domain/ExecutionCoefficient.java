package au.com.wp.corp.gia.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "EXECUTION_COEFFICIENT")
public class ExecutionCoefficient {

	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "EXECUTION_COEFFICIENT_KEY", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	@Column(name = "EXECUTION_COEFFICIENT_KEY", nullable = false)
	private Long id;
	
	@Column(name="MODEL_EXECUTION_KEY", nullable = false)
	private Long modelExecutionKey;
	
	@Column(
	        name="LHS_COEFFICIENT_HISTORY_KEY",
	        nullable=false,
	        unique=true,
	        columnDefinition = "NUMBER DEFAULT nextval('LHS_COEFFICIENT_HISTORY_KEY')"
	)
	@Generated(GenerationTime.INSERT)
	private Long lhsCoefficientHistKey;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getModelExecutionKey() {
		return modelExecutionKey;
	}

	public void setModelExecutionKey(Long modelExecutionKey) {
		this.modelExecutionKey = modelExecutionKey;
	}

	public Long getLhsCoefficientHistKey() {
		return lhsCoefficientHistKey;
	}

	public void setLhsCoefficientHistKey(Long lhsCoefficientHistKey) {
		this.lhsCoefficientHistKey = lhsCoefficientHistKey;
	}
}
