package au.com.wp.corp.gia.domain;

import static au.com.wp.corp.gia.domain.DbConstants.C_EQUATION_KEY;

import javax.persistence.*;

import static javax.persistence.InheritanceType.JOINED;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;

import static javax.persistence.DiscriminatorType.STRING;
import static au.com.wp.corp.gia.domain.DbConstants.SCHEMA;
import static au.com.wp.corp.gia.domain.DbConstants.C_EQUATION_KEY;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GenerationType;

import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "LHS_COEFFICIENT")
public class LHSCoefficient {
	@Id
	@GeneratedValue(generator = "LHS_COEFFICIENT_KEY")
	@GenericGenerator(strategy = "sequence", name = "LHS_COEFFICIENT_KEY", parameters = {
			@Parameter(name = "sequence", value = "LHS_COEFFICIENT_KEY") })
	@Column(name = "LHS_COEFFICIENT_KEY", nullable = false)
	private Long lhsCoeffKey;

	@ManyToOne
	@JoinColumn(name = "CONSTRAINT_EQUATION_KEY")
	private ConstraintEquation constraintEquation;

	@Column(name = "VARIABLE_KEY", nullable = false)
	private Long variableKey;
	
	@Column(name = "VALUE", nullable = false)
	private Double coefficient;
	
	@Column(name = "LAST_MODIFIED_TIMESTAMP", nullable = false)
	private Timestamp lastModifiedTimeStamp;
	
	@Column(name = "LAST_MODIFIED_BY", nullable = false)
	private String lastModifiedBy;
	
	@Column(
	        name="CURRENT_HISTORY_KEY",
	        nullable=false,
	        unique=true,
	        columnDefinition = "NUMBER DEFAULT nextval('LHS_COEFFICIENT_HISTORY_KEY')"
	)
	@Generated(GenerationTime.INSERT)
	private Long currentHistoryKey;

	public ConstraintEquation getConstraintEquation() {
		return constraintEquation;
	}

	public void setConstraintEquation(ConstraintEquation constraintEquation) {
		this.constraintEquation = constraintEquation;
	}

	public Long getVariableKey() {
		return variableKey;
	}

	public void setVariableKey(Long variableKey) {
		this.variableKey = variableKey;
	}

	

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}

	public Timestamp getLastModifiedTimeStamp() {
		return lastModifiedTimeStamp;
	}

	public void setLastModifiedTimeStamp(Timestamp lastModifiedTimeStamp) {
		this.lastModifiedTimeStamp = lastModifiedTimeStamp;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Long getCurrentHistoryKey() {
		return currentHistoryKey;
	}

	public void setCurrentHistoryKey(Long currentHistoryKey) {
		this.currentHistoryKey = currentHistoryKey;
	}

}
