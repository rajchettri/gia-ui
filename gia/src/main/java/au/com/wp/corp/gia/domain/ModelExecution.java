package au.com.wp.corp.gia.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="MODEL_EXECUTION")
public class ModelExecution {

	@Id
	@SequenceGenerator(name = "sequence", sequenceName = "MODEL_EXECUTION_KEY", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	@Column(name = "MODEL_EXECUTION_KEY", nullable = false)
	private Long id;
	
	@Column(name = "EXECUTION_START_TIMESTAMP", nullable = false)
	private Timestamp executionStartTimestamp;
	
	@Column(name = "EXECUTION_END_TIMESTAMP", nullable = true)
	private Timestamp executionEndTimestamp;
	
	@Column(name = "RETURN_CODE", nullable = true)
	private Long returnCode;
	
	@Column(name = "RETURN_CODE_TXT", nullable = true)
	private String returnCodeTxt;
	
	@Column(name = "NUMBER_BOUND_CONSTRAINTS", nullable = true)
	private Long numberBoundConstraints;
	
	@Column(name = "STATUS", nullable = false)
	private String status;
	
	@Column(name = "DISPATCHER_ADVISORY_ID", nullable = true)
	private String dispatcherAdvisoryId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getExecutionStartTimestamp() {
		return executionStartTimestamp;
	}

	public void setExecutionStartTimestamp(Timestamp executionStartTimestamp) {
		this.executionStartTimestamp = executionStartTimestamp;
	}

	public Timestamp getExecutionEndTimestamp() {
		return executionEndTimestamp;
	}

	public void setExecutionEndTimestamp(Timestamp executionEndTimestamp) {
		this.executionEndTimestamp = executionEndTimestamp;
	}

	public Long getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Long returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnCodeTxt() {
		return returnCodeTxt;
	}

	public void setReturnCodeTxt(String returnCodeTxt) {
		this.returnCodeTxt = returnCodeTxt;
	}

	public Long getNumberBoundConstraints() {
		return numberBoundConstraints;
	}

	public void setNumberBoundConstraints(Long numberBoundConstraints) {
		this.numberBoundConstraints = numberBoundConstraints;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispatcherAdvisoryId() {
		return dispatcherAdvisoryId;
	}

	public void setDispatcherAdvisoryId(String dispatcherAdvisoryId) {
		this.dispatcherAdvisoryId = dispatcherAdvisoryId;
	}
}
