/**
 * 
 */
Gia.directive('giaHeader', [ function(){
    
    return {
       restrict: 'E',
       replace: true,
       templateUrl: "views/header.html",
       controller: ['$scope', '$filter', '$state', '$rootScope', '$window', 'ValueObject','$localStorage',function ($scope, $filter, $state, $rootScope, $window,ValueObject,$localStorage) {
    	   $scope.userName=$localStorage.userName;
    	  $scope.userRole=$localStorage.userRole;
    	   $scope.operational=function(){
    	        $state.go('operationalui');
    	   }
                

       }]
   }
    
}]);