/**
 * 
 */
Gia.directive('menuTabs', [ function(){
    
    return {
       restrict: 'E',
       replace: true,
       templateUrl: "views/menu.html",
       controller: ['$scope', '$filter', '$state', '$rootScope', '$window', function ($scope, $filter, $state, $rootScope, $window) {
    	   $state.go('testplatform.constraintManagement');
    	   $scope.selectedTabIndex=3;
$scope.callTab=function(tabValue,tabIndex){
	$state.go(tabValue);
	$scope.selectedTabIndex=tabIndex;
}
                

       }]
   }
    
}]);