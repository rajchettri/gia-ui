Gia.filter('truncate', function() {
	return function(input) {
		if (input === undefined)
		{
			return '';
		}
		var output;
		if (input.length > 30) {
			output = input.substr(0, 30) + "...";
		} else {
			output = input;
		}

		return output;
	};
})
