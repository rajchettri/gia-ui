        var Gia = angular.module('angularspring', [ 'ui.router','ngStorage', 'ui.bootstrap', 'ui.grid', 'ui.grid.selection','ui.grid.pagination','ui.grid.resizeColumns', 'ui.grid.moveColumns','angularModalService','ngMaterial','ngAnimate', 'ngSanitize','ngResource','ngMessages','ngTable']);

    Gia
    		.config([
    				'$stateProvider',
    				'$urlRouterProvider',
    				'$httpProvider',
    				function($stateProvider, $urlRouterProvider, $httpProvider) {
    					$urlRouterProvider.otherwise('/landing');
    					$stateProvider.state('landing', {
    						url : '/landing',
    						templateUrl : 'views/landing.html',
    						controller : 'landingCtrl'
    					}).state('testplatform', {
    						url : '/testplatform',
    						templateUrl : 'views/testplatform.html',
    						controller : 'testPlatformCtrl'
    					}).state('testplatform.constraintManagement', {
    						url : '/constraintmanagement',
    						templateUrl : 'views/TestPlatformPartials/constraintManagement.html',
    						controller : 'constraintMCtrl'
    					}).state('testplatform.constraintLibrary', {
    						url : '/constraintlibrary',
    						templateUrl : 'views/TestPlatformPartials/constraintLibrary.html',
    						controller : 'constraintLCtrl'
    					}).state('testplatform.referenceData', {
    						url : '/referencedata',
    						templateUrl : 'views/TestPlatformPartials/referenceData.html',
    						controller : 'ReferenceCtrl'
    					}).state('operationalui', {
    					     url : '/operationalui',
    					     templateUrl : 'views/OperationalUIPartials/operational.html'
    					     
    					 }).state('menu.search', {
    						url : '/search',
    						templateUrl : 'searchImport.html',
    						controller : 'searchimportCtrl'
    					}).state('menu.addtemplate', {
    						url : '/addtemplate',
    						templateUrl : 'addTemplate.html',
    						controller : 'addTemplateCtrl'
    					}).state('menu.existtemplate', {
    						url : '/existtemplate',
    						templateUrl : 'existingTemplate.html',
    						controller : 'existTemplateCtrl'
    					});
    					if (!$httpProvider.defaults.headers.get) {
    						$httpProvider.defaults.headers.get = {};
    					}
    					$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    					$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    					$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    				} ]);

    Gia.value('ValueObject', {
    	userRole : 'title',
    	userName: 'username',
    	logOut:false
    });

    Gia.directive('isNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope) {    
                scope.$watch('existemplate.version', function(newValue,oldValue) {
                   var arr = String(newValue).split("");
                   if (arr.length === 0) return;
                   if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                   if (arr.length === 2 && newValue === '-.') return;
                   if (isNaN(newValue)) {
                       scope.existemplate.version = oldValue;
                   }
               });
            }
        };
    });