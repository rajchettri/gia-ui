
(function(window) {
  window.__env = window.__env || {};

  // API url
  window.__env.serverUrl = 'http://localhost:8080/gia/';
  window.__env.maxActiveMirrors = 5;
  window.__env.maxMirrors = 20;
}(this));
