Gia.controller('modalCtrl', [
							'$scope',
							'$http',
							'$location',
							'$anchorScroll',
							'$timeout',
							'$rootScope',
							'close',
							function($scope, $http, $location, $anchorScroll,
									$timeout, $rootScope, close) {
$scope.message=$rootScope.message;
$scope.showHeaderTitle=$rootScope.showHeaderTitle;
								$scope.noSave=$rootScope.noSave;
								$scope.close = function(result) {
									close(result, 500); // close, but give 500ms for bootstrap to animate
								};
							} ]);