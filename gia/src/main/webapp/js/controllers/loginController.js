Gia.controller('loginCtrl', [
		'$scope',
		'$http',
		'$state',
		'$rootScope',
		'loginService',
		'ValueObject',
		function($scope, $http, $state, $rootScope, loginService, ValueObject) {
			$scope.credential={};
			$scope.credential.UserName = '';
			$scope.credential.password = '';
			$scope.logininvalid = false;
			$rootScope.showdiv = false;
			$rootScope.loggedout=true;
			$scope.validateUser = function() {

				loginService.getData($scope.credential).then(
						function(response) {
							if (response.validated == true) {
								$state.go('landing');
								$scope.logininvalid = false;
								//$rootScope.version = response.appVersion;
								$rootScope.userNameHead = response.userName;
								$rootScope.showdiv = true;
								$rootScope.loggedout=false;
								userRole.title = response.userType;
							} else {
								$scope.logininvalid = true;
								$state.go('landing');
								$scope.credential.userName = '';
								$scope.credential.password = '';
								$rootScope.loggedout=true;
								$rootScope.showdiv = false;
							}

						}).catch(function(fallback){
							
							$state.go('landing');
							ValueObject.logOut=false;
						})

				
			}
			
		} ]);
