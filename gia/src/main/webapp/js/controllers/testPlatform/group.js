'use strict';

/**
 * @ngdoc function
 * @name giaAngularApp.controller:GroupctrlCtrl
 * @description
 * # GroupctrlCtrl
 * Controller of the giaAngularApp
 */
Gia.controller('GroupCtrl', ['$scope', 'GroupApi', '$controller', function($scope, GroupApi, $controller) {
  $scope.closed = true; // change to true when done testing
  var deleteVal = function() {
    console.log("xe");
    if ($scope.record.hasEquation) {
      window.alert("This group cannot be deleted as it is currently in use by another user.");
      return false;
    } else {
      return true;
    }
  }
  $scope.getParams = function(v) {
    return {
      constraintGroupKey: v
    };
  }
  $controller('DataManagerCtrl', {
    Api: GroupApi,
    primaryKeyName: 'constraintGroupKey',
    recordName: 'group',
    deleteValidation: deleteVal,
    $scope: $scope
  });



  $scope.validateUniqueness = function() {


    var name = true;
    angular.forEach($scope.list, function(v) {
      // don't validate uniqueness against itself.
      if ($scope.record.constraintGroupKey !== v.constraintGroupKey) {


        if (v.groupName === $scope.record.groupName) {
          name = false;
        }

        $scope.form.groupName.$setValidity('unique', name);

      }

    });
  };






}]);
