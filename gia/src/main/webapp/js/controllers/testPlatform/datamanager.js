'use strict';

/**
 * @ngdoc function
 * @name giaAngularApp.controller:DatamanagerCtrl
 * @description This controller i sthe parent controller for VariableCtrl,
 *              GroupCtrl and PlatormCtrl. It abstracts out the logic required
 *              to manage the form and data table. # DatamanagerCtrl Controller
 *              of the giaAngularApp
 */
Gia.controller('DataManagerCtrl', ['Api', 'primaryKeyName', 'recordName', 'deleteValidation', '$scope', '$parse', 'NgTableParams', '$filter', function(Api, primaryKeyName, recordName, deleteValidation, $scope, $parse, NgTableParams, $filter) {
  $scope.closed = false;

  $scope.resetForm = function() {};
  $scope.CREATE_UPDATE_WARNING = function() {
    return 'Please confirm that all the details you have entered are correct. Any changes in this ' + recordName + ' section will have an immediate effect on the GIA Engine and all Test Platforms, possibly causing it to fail.';
  };
  $scope.DELETE_WARNING_MESSAGE = function() {
    return 'Are you sure you want to DELETE the selected ' + recordName + '? This deletion shall have an immediate effect on the GIA Engine and all Test Platforms, possibly causing it to fail. Once deleted this record cannot be restored.';
  };

  $scope.DELETE_ERROR_MESSAGE = function() {
    return 'There was a problem deleting the ' + recordName;
  };
  $scope.DELETE_SUCCESS_MESSAGE = function() {
    return 'The ' + recordName + ' has been deleted successfully.';
  };
  $scope.CREATE_UPDATE_ERROR = function() {
    return 'There was a problem creating or updating the ' + recordName + '.';
  };
  $scope.CREATE_UPDATE_SUCCESS = function() {
    return 'The ' + recordName + ' has been ' + (($scope.editing) ? 'updated' : 'created') + ' successfully';
  };
  // This is used to determine whether user is entering a new item or editing
  // an existing one.
  $scope.editing = false;
  $scope.primaryKey = primaryKeyName;
  $scope.list = [];
  $scope.updateTable = function() {

    return Api.query(function(data) {
      $scope.list = data;

      if (recordName === 'variable') {
        console.log("sorting by variable");
        $scope.list = $filter('orderBy')(data, 'name', false);
      } else if (recordName === 'platform') {
        $scope.list = $filter('orderBy')(data, 'mirrorKey', false);
      } else if (recordName === 'group') {
        $scope.list = $filter('orderBy')(data, 'constraintGroupName', false);
      }
      $scope.tableParams = new NgTableParams({
        count: 100
      }, {
        dataset: $scope.list
      });



      console.log($scope.list)
      $scope.tableParams.reload();
    }, function(status, error) {
      console.log("Error fetching data from " + recordName + " API: " + status.statusText);
      console.error(status);
    });
  };

  $scope.updateTable();


  $scope.set = function(v) {
    // var the_id = $parse(primaryKeyName);
    console.log("V: " + v);
    if (!angular.isUndefined(v)) {
      console.log("Fetching record..");
      $scope.record = Api.get($scope.getParams(v), function(data) {
        $scope.record = data;
        // saved record id in seperate var because users might change the record
        // id in the form. Required for not validating uniqueness against the
        // record itself.
        $scope.record_id = data[primaryKeyName];
        console.log($scope.record_id);
        $scope.resetForm();
        if (recordName === 'platform') {
          $scope.editingSimulated = ($scope.record.mirrorStatus === "Simulated");
          console.log('set editingSimulated to ' + $scope.editingSimulated);
        }
        $scope.editing = true;

      }, function(error) {
        window.alert("Error fetching data: " + error);
      });

    } else {
      $scope.record = new Api();
      $scope.editing = false;
      if (recordName === 'platform') {
        $scope.editingSimulated = false;
      }
      if (recordName === 'variable') {
        $scope.record.dummyFlag = 'F';
      }
      console.log("Creating new record..");

      $scope.resetForm();
    }
  };
  $scope.set();
  $scope.createUpdate = function(success, failure) {
    if (window.confirm($scope.CREATE_UPDATE_WARNING())) {
      var f = function(error) {
        window.alert($scope.CREATE_UPDATE_ERROR());
        console.error(error);
      };

      var s = function(data) {
        $scope.record = data;
        window.alert($scope.CREATE_UPDATE_SUCCESS());
        $scope.set();
        $scope.updateTable();
        $scope.resetForm();
      };

      if ($scope.editing) {
        $scope.record.$save(s, f);
      } else {
        $scope.record.$save(s, f);
      }
    }
  };

  $scope.cancel = function() {
    $scope.set();
    $scope.resetForm();
  };

  $scope.delete = function() {

    if (window.confirm($scope.DELETE_WARNING_MESSAGE())) {
      if (deleteValidation()) {
        console.log('Deleting...');
        console.log($scope.record);
        $scope.record.$delete(function() {
          window.alert($scope.DELETE_SUCCESS_MESSAGE());
          $scope.updateTable();
          $scope.resetForm();
          $scope.set();
        }, function(error) {
          window.alert($scope.DELETE_ERROR_MESSAGE() + error.statusText);
          console.error(error);
        });

      }
    }
  };

  $scope.resetForm = function() {
    $scope.form.$setUntouched();
    $scope.form.$setPristine();
  };


}]);
