'use strict';

/**
 * @ngdoc function
 * @name giaAngularApp.controller:PlatformCtrl
 * @description
 * # PlatformCtrl
 * Controller of the giaAngularApp
 */
Gia.controller('PlatformCtrl', ['$scope', 'PlatformApi', '$controller', function($scope, PlatformApi, $controller) {
  var deleteVal = function() {
    return false; // platforms cannot be deleted
  }
  $scope.getParams = function(v) {
    return {
      mirrorKey: v
    };
  }
  $controller('DataManagerCtrl', {
    Api: PlatformApi,
    primaryKeyName: 'mirrorKey',
    recordName: 'platform',
    deleteValidation: deleteVal,
    $scope: $scope
  });

  //$scope.opPlatform = $scope.set(0);
  $scope.closed = true; // change to true when done testing

  $scope.CREATE_UPDATE_WARNING = function() {
    return 'Please confirm that all the details you have entered are correct.';
  };
  $scope.CREATE_UPDATE_SUCCESS = function() {
    return "Platform " + (($scope.editing) ? ' updated' : ' created') + " successfully.";
  };

  var initPlatform = function() {
    $scope.editing = false;
    $scope.record = new PlatformApi();
    $scope.record.mirrorKey = $scope.platforms.length;

  };

  // Overwrite method because we need to enforce the maximum number of enabled platforms.
  $scope.createUpdate = function(success, failure) {
    if (window.confirm($scope.CREATE_UPDATE_WARNING())) { // confirm inputs are correct
      var f = function(error) {
        window.alert($scope.CREATE_UPDATE_ERROR());
        console.error(error.statusText);
      };

      var s = function() {
        window.alert($scope.CREATE_UPDATE_SUCCESS());
        $scope.set();
        $scope.updateTable();
        $scope.resetForm();
      };

      console.log($scope.list);
      if ($scope.editing || $scope.list.length <= __env.maxMirrors) {
        if ($scope.record.mirrorStatus === 'Simulated' && !$scope.editingSimulated && numEnabled()) {
          window.alert("Please change the status to Disabled before saving this as the maximum number (" + __env.maxActiveMirrors + ") of active Test Platforms has been reached OR you can disabled other Test Platforms instead.");
        } else {
          $scope.record.$save(s, f);
        }
      } else {
        window.alert("The maximum number (" + __env.maxMirrors + ") of Test Platform has been reached. You can no longer create a new Test Platform but you may edit on existing Test Platforms.");
      }
    }
  };

  $scope.platformNumber = function() {
    if ($scope.editing) {
      return $scope.record.mirrorKey;
    } else {
      if ($scope.list.length <= __env.maxMirrors) {
        return $scope.list.length;
      } else {
        return '';
      }
    }

  };

  // Counts number of enabled mirrors
  var numEnabled = function() {
    return $scope.list.reduce(function(n, record) {
        return n + (record.mirrorStatus === 'Simulated');
      }, 0) >= __env.maxActiveMirrors;
  };





}]);
