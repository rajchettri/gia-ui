'use strict';
/**
 * @ngdoc function
 * @name giaAngularApp.controller:VariableCtrl
 * @description
 * # VariableCtrl
 * Controller of the giaAngularApp
 */
Gia.controller('VariableCtrl', ['$scope', 'VariableApi', '$controller', function($scope, VariableApi, $controller) {
  var deleteVal = function() {
    if ($scope.record.inUse) {
      window.alert("This variable cannot be deleted as it is currently in use by another user.");
      return false;
    } else {
      return true;
    }
  }
  $scope.getParams = function(v) {
    return {
      variableKey: v
    };
  }
  $controller('DataManagerCtrl', {
    Api: VariableApi,
    primaryKeyName: 'variableKey',
    recordName: 'variable',
    deleteValidation: deleteVal,
    $scope: $scope
  });
  // $scope.gridOptions.columnDefs = [
  //   {
  //     name: 'Variable Name',
  //     cellTemplate: '<a ng-click="grid.appScope.set(row.entity.variableKey)">{{row.entity.name}}</a>'
  //   }
  // ]
  $scope.CREATE_UPDATE_WARNING = function() {
    return 'Please confirm that all the details you have entered are correct. Any changes in this Variable Data Mapping section will have an immediate effect on the GIA Engine and all Test Platforms, possibly causing it to fail.';
  };
  $scope.DELETE_WARNING_MESSAGE = function() {
    return 'Are you sure you want to DELETE the selected variable ' + $scope.record.alias + '? This deletion shall have an immediate effect on the GIA Engine and all Test Platforms, possibly causing it to fail. Once deleted this variable cannot be restored.';
  };
  $scope.giaFlagOptions = [{
    id: 'S',
    label: 'Scheduled'
  }, {
    id: 'NS',
    label: 'Non-Scheduled'
  }, {
    id: 'NA',
    label: 'Not Applicable'
  }];


  var closed = true; // change to false when done testing



  $scope.validateUniqueness = function() {

    var name = true;
    var facilityName = true;
    var alias = true;
    var scadaActualPiTag = true;
    var scadaActualPiQualityTag = true;
    var scadaForecastPiTag = true;
    var scadaForecastPiQualityTag = true;
    var stateEstimatedPiTag = true;
    var xaOverridePiTag = true;

    angular.forEach($scope.list, function(v) {
      // don't validate uniqueness against itself.
      if ($scope.record.variableKey !== v.variableKey) {
        //console.log("Record ID:\t" + $scope.variableKey + "\tCheck ID: " + v.variableKey);
        // TODO rafactor these by removing if statements and assigning boolean condition directly to variables
        if (v.name === $scope.record.name) {
          name = false;
        }
        if (v.alias === $scope.record.alias) {
          alias = false;
        }

        if (v.facilityName && v.facilityName === $scope.record.facilityName) {
          facilityName = false;
        }

        if ($scope.record.scadaActualPiTag && v.scadaActualPiTag === $scope.record.scadaActualPiTag) {
          scadaActualPiTag = false;
          console.log("Invalid");
        }

        if ($scope.record.scadaActualPiQualityTag && v.scadaActualPiQualityTag === $scope.record.scadaActualPiQualityTag) {
          scadaActualPiQualityTag = false;
        }
        if (v.giaFlag === 'NS') {
          if ($scope.record.scadaForecastPiTag && v.scadaForecastPiTag === $scope.record.scadaForecastPiTag) {
            scadaForecastPiTag = false;
          }

        }

        if (v.giaFlag === 'NS') {

          if ($scope.record.scadaForecastPiQualityTag && v.scadaForecastPiQualityTag === $scope.record.scadaForecastPiQualityTag) {
            scadaForecastPiQualityTag = false;
          }

        }
        if ($scope.record.stateEstimatedPiTag && v.stateEstimatedPiTag === $scope.record.stateEstimatedPiTag) {
          stateEstimatedPiTag = false;
        }

        if ($scope.record.xaOverridePiTag && v.xaOverridePiTag === $scope.record.xaOverridePiTag) {
          xaOverridePiTag = false;
        }


        $scope.form.name.$setValidity('unique', name);
        $scope.form.alias.$setValidity('unique', alias);
        $scope.form.facilityName.$setValidity('unique', facilityName);
        $scope.form.scadaActualPiTag.$setValidity('unique', scadaActualPiTag);
        $scope.form.scadaActualPiQualityTag.$setValidity('unique', scadaActualPiQualityTag);
        $scope.form.scadaForecastPiTag.$setValidity('unique', scadaForecastPiTag);
        $scope.form.scadaForecastPiQualityTag.$setValidity('unique', scadaForecastPiQualityTag);
        $scope.form.stateEstimatedPiTag.$setValidity('unique', stateEstimatedPiTag);
        $scope.form.xaOverridePiTag.$setValidity('unique', xaOverridePiTag);


      }

    });

  };

  $scope.convertFlag = function(flag) {
    if (flag === 'S') {
      return 'Scheduled';
    } else if (flag === 'NS') {
      return 'Not Scheduled';
    } else if (flag === 'NA') {
      return 'Not Applicable';
    }
  };

  $scope.kpiGraphURL = function(sinusoid) {
    var url = "http://dmzpiweb:8170/VisualKPI/trend.aspx?totime=*&fromtime=*-7+Days&pn=1&trendtype=0&P0=0&A0=Pen%201&S0=1&I0=66b13a22-2808-4dfe-9e9d-ff1c8d2816e2&O0=&minY=&maxY=&L0=" + sinusoid;
    window.open(url, "popup", "");
  };


}]);
