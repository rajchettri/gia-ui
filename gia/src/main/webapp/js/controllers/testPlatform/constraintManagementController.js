Gia.controller('constraintMCtrl', ['$scope', '$http', '$location', '$anchorScroll', '$rootScope', 'ModalService','$filter','$element','$timeout','constraintMService',
        function($scope, $http,$location,$anchorScroll, $rootScope,ModalService,$filter,$element,$timeout,constraintMService){
	$scope.duplicateCE=false;
	$scope.gia={};
	$scope.submitted=false;
	$scope.ceForm={};
	$rootScope.showHeaderTitle=true;
	$rootScope.noSave=false;
	var Count=0;
var rowId=[];
$scope.publishData=[];
var counter=0;
	$scope.dynamicTooltipText = '<span uib-tooltip-html="new <br/> old <br/>">Status?</span>'
	var copyVarDropdown=[];
	$scope.clickedRow=false;
	$scope.saveBtn=function(gia){
		$scope.duplicateCE=false;
		$scope.submitted=true;
		$scope.emptyValue=false;
		if($scope.gridOptions.data.length==0){
			$scope.emptyValue=true;
			
		}
		if($scope.ceForm.$invalid){
			$scope.submitted=true;
		}
		else{
			constraintMService.getData($scope.gia).then(
					function(data) {

						$scope.submitted=false;
						
						if($scope.gridOptions.data.length==0){
							$scope.emptyValue=true;
							
						}
						else if($scope.gia.eqn && $scope.gia.desc && $scope.gia.constraintGrpKey && $scope.gia.rhs && $scope.gia.comm && $scope.gia.oper && $scope.gridOptions.data){
					if(data.isCePresent=='T' ){
						$scope.duplicateCE=true;
						$scope.emptyValue=false;
					}
					else{
					$scope.gridOptions1.data=data;
					$scope.gridOptions.data=[];
					for(var i=0;i<$scope.gia.coefficientKeyValue.length;i++){
						
						var index= _.findIndex($scope.uploadDropdown3,{'lhskey':$scope.gia.coefficientKeyValue[i].lhskey});
						if(index==-1){
							$scope.uploadDropdown3.push({'lhskey':$scope.gia.coefficientKeyValue[i].lhskey,'variable':$scope.gia.coefficientKeyValue[i].variable});
							}
						}
					$scope.gia={};


						$scope.emptyValue=false;
					//$scope.uploadDropdown3=$scope.variableDropdown;
					console.log(data);
					$rootScope.message=data.slice(-1)[0].statusMsg;
					//data=data.pop(_.last($scope.gridOptions1.data));
					$rootScope.showHeaderTitle=false;
					$rootScope.noSave=true;
					$scope.gridOptions1.data.pop()
						ModalService.showModal({
						    templateUrl: 'views/modal.html',
						    controller: "modalCtrl"
						}).then(function(modal) {
						    modal.element.modal();
						    modal.close.then(function(result) {
						   	 
						    });
						});
					}
					}
					
					}).catch(function(fallback){
						
					})
	}
	}
	$scope.cloneBtn=function(){
		$scope.gia.id='';
		$scope.gia.version='';
		$scope.gia.eqn='';
		$scope.editedCE=false;
		$scope.duplicateCE=false;
	}

	$scope.disableClone=function(){
		if($scope.gia.id){
	        return false;
	  }
	  else{
	        return true
	  }
	}

	$scope.deleteBtn=function(){
		$scope.duplicateCE=false;
		$scope.editedCE=true;
		$rootScope.message="Are you sure you want to DELETE the selected constraint equation?";
		$rootScope.showHeaderTitle=true;
		$rootScope.noSave=false;
		 ModalService.showModal({
	         templateUrl: 'views/modal.html',
	         controller: "modalCtrl"
	     }).then(function(modal) {
	         modal.element.modal();
	         modal.close.then(function(result) {
	        	 if(result.toLowerCase().trim()=='yes'){
	        		 constraintMService.getDeleteCEPushed($scope.gia.id).then(
	     					function(data) {
	     					 $scope.gridOptions1.data=data;
        		 			$scope.gridOptions.data=[];
        		 $scope.editedCE=false;
        		 $scope.submitted=false;
        		 for(var i=0;i<$scope.gia.coefficientKeyValue.length;i++){
        				
        				var index= _.findIndex($scope.uploadDropdown3,{'lhskey':$scope.gia.coefficientKeyValue[i].lhskey});
        				if(index==-1){
        					$scope.uploadDropdown3.push({'lhskey':$scope.gia.coefficientKeyValue[i].lhskey,'variable':$scope.gia.coefficientKeyValue[i].variable});
        					}
        				}
        		 console.log(data);
        		 $scope.gia={};
	     					
	     					
	     					}).catch(function(fallback){
								
							})
	        	
	        	 }
	         });
	     });

	 
	}

$scope.publishBtn=function(){ 
	$rootScope.message="Are you sure you want to PUBLISH the selected constraint equation/s?";
	$rootScope.showHeaderTitle=true;
	$rootScope.noSave=false;
	ModalService.showModal({
		templateUrl: 'views/modal.html',
		controller: "modalCtrl"
	}).then(function(modal) {
		modal.element.modal();
		modal.close.then(function(result) {
			if(result.toLowerCase().trim()=='yes'){
				if($scope.gia.length!==0 &&  $scope.gia.length!==undefined && $scope.publishData.length==0){
					$scope.saveBtn($scope.gia);
					$scope.publishData=$scope.gia;
					$http.post('./publish',$scope.publishData).success(function(data) {
					})
				}
				else if($scope.gia.length!==0 &&  $scope.gia.length!==undefined && $scope.publishData.length!==0){
					$scope.saveBtn($scope.gia);
					$scope.publishData.unshift($scope.gia);
					$scope.publishData=_.uniqBy($scope.publishData,$scope.gia.id);
				}
				else{
					$http.post('./publish',$scope.publishData).success(function(data) {
					$scope.gridOptions1.data=data;
					console.log(data);
					}) 
				} 
			}
		});
	});



}



	constraintMService.onLoad().then(
				function(data) {
					console.log(data);
					$scope.uploadDropdown3=[];
					$scope.uploadDropdown2=_.sortBy(data.constraint.gia, [function(o) { return o.group; }]);
					$scope.uploadDropdown3=data.constraint.CoeffKey;
					$scope.variableDropdown= data.constraint.CoeffKey;
					copyVarDropdown=_.cloneDeep($scope.uploadDropdown3);
					$scope.gridOptions1.data=data.list;
					$scope.searchTerm;
					$timeout( function(){
					angular.element(document.getElementsByClassName('TooltipCatcher')).attr("title","NEW: New CE* \n EDIT: Edited CE* \n DEL: Deleted CE* \n PUB: Published CE to Operational UI \n *Any CEs that are not in the PUB state means their changes is not reflected in the Operational UI." );
					}, 3000 );
					$scope.clearSearchTerm = function() {
					  $scope.searchTerm = '';
					};
				}).catch(function(fallback){
				
			})

	   $scope.addBtnName='Add';
	$scope.deleteRow = function(row) {
		$rootScope.message="Please confirm you want to delete the selected entry.";
		$rootScope.showHeaderTitle=true;
		$rootScope.noSave=false;
		 ModalService.showModal({
	         templateUrl: 'views/modal.html',
	         controller: "modalCtrl"
	     }).then(function(modal) {
	         modal.element.modal();
	         modal.close.then(function(result) {
	        	 if(result.toLowerCase().trim()=='yes'){
	        		 var index = $scope.gridOptions.data.indexOf(row.entity);
	        		 $scope.gia.coefficientKeyValue='';
	      			$scope.gia.coefficient='';
	      			$scope.gia.variableValue='';
	      			$scope.addBtnName='Add';
	      			$scope.index=-2;
	        		 $scope.gridOptions.data.splice(index, 1);
	        		 $scope.gridOptions.data= _.sortBy($scope.gridOptions.data, [function(o) { return o.variable; }]);
	        		 $scope.gia.coefficientKeyValue=$scope.gridOptions.data;
	        		 var index=_.findIndex($scope.uploadDropdown3,{'lhskey':row.entity.lhskey});
	        		 if(index==-1){
	        		 $scope.uploadDropdown3.push(row.entity);
	        		 
	        		 
	        		 }
	        	 }
	         });
	     });

	};
	$scope.changeVariable=function(value){
		$scope.gia.variableValue= value.variable;
		$scope.gia.coefficientKey=value.lhskey;
		$scope.gia.coefficientKeyValue=value;
		if($scope.index>-1){
		$scope.addBtnName='Edit';
		}else{
			$scope.addBtnName='Add';
			}
		}


	$scope.addBtndisable=function(){
		if($scope.gia && $scope.gia.variableValue  && $scope.gia.coefficient ){
			return false;
		}
		else{
			return true;
		}
	}

	/************************Start Date**************************************/
	$scope.timeChanged = false;
	$scope.startDateValid = false;
	$scope.endDateValid = false;
	 /*  $scope.gia.startDate = $filter('date')(new Date(), 'dd/MM/yyyy');
	$scope.gia.endDate =$filter('date')("31/12/2099",'dd/MM/yyyy') */ 
	var date1=new Date();

	var dateParts3=$filter('date')("31/12/2099",'dd/MM/yyyy');
	dateParts3=dateParts3.split("/");
	var date3= new Date(dateParts3[2], dateParts3[1] - 1, dateParts3[0]);
	 var date2 = new Date();
	date2.setDate(date2.getDate()-1);
	$scope.changedStartDate = function() {
		if(this.gia.startDate) {


			var dateParts = $scope.gia.startDate.split("/");
			date1=new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
		   
			
		   if(date1.getTime()>date2.getTime() ){
			   
		   if(date1.getTime()<date3.getTime()) {
		   	$scope.startDateValid = false;
		   	$scope.endDateValid = false;
		   }
		   else{
			   $scope.startDateValid = false;
			   $scope.endDateValid = true;
		   }
		   }
		   else {
		   	$scope.startDateValid = true
		   	$scope.endDateValid = false;
		   }
		   }
		}



	 $scope.changedEndDate = function() {
	 if( this.gia.endDate) {
	 	 var dateParts = $scope.gia.endDate.split("/");

	 date3 = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

	   if(date3.getTime()>date1.getTime()) {
	   	$scope.endDateValid = false;
	   }
	   else {
	   	$scope.endDateValid = true;
	   }
	   }
	 }



	/*********************End date*****************************************/



	$scope.gridOptions=[];
	$scope.editedCE=false
	$scope.ceName=function(){
		if($scope.editedCE){
			return true;
		}
		return false;
	}
	$scope.add = function() {
		$scope.emptyValue=false;
		if($scope.addBtnName=='Edit'){
		
		
			
				
			
			$scope.gridOptions.data.splice($scope.index,1);
			
			$scope.index=-2;
			
			$scope.gridOptions.data.push({"variable":$scope.gia.variableValue,"coefficient":$scope.gia.coefficient,"lhskey":$scope.gia.coefficientKey});
			var filter= _.filter($scope.gridOptions.data,{"lhskey":$scope.gia.coefficientKey});
			if(filter.length>1){
				
			for(var i=0;i<filter.length;i++){
				
				var indexFilter= _.findIndex($scope.gridOptions.data,{'lhskey':filter[i].lhskey})
				$scope.gridOptions.data.splice(indexFilter,1);
			}
			$scope.gridOptions.data.push({"variable":$scope.gia.variableValue,"coefficient":$scope.gia.coefficient,"lhskey":$scope.gia.coefficientKey});
			}
			/* else{
				$scope.gridOptions.data.splice($scope.index,1)
			} */
			$scope.gridOptions.data = _.sortBy($scope.gridOptions.data, [function(o) { return o.variable; }]);
				$scope.gia.coefficient='';
			$scope.gia.variableValue='';
				var index= _.findIndex($scope.uploadDropdown3,{'lhskey':$scope.gia.coefficientKey})
				
				$scope.uploadDropdown3.splice(index,1);
				
				$scope.addBtnName='Add';
				$scope.gia.coefficientKeyValue=$scope.gridOptions.data;
				for(var i=0;i<$scope.gia.coefficientKeyValue.length;i++){
					
					var index= _.findIndex($scope.uploadDropdown3,{'lhskey':$scope.gia.coefficientKeyValue[i].lhskey});
					if(index!==-1){
						$scope.uploadDropdown3.splice(index,1);
						}
					}
		}
		else{
			$scope.gridOptions.data.push({"variable":$scope.gia.variableValue,"coefficient":$scope.gia.coefficient,"lhskey":$scope.gia.coefficientKey});
			$scope.gia.coefficientKeyValue=$scope.gridOptions.data;
			$scope.gia.coefficient='';
			$scope.gia.variableValue=''
			var index= _.findIndex($scope.uploadDropdown3,{'lhskey':$scope.gia.coefficientKey})
			$scope.gridOptions.data= _.sortBy($scope.gridOptions.data, [function(o) { return o.variable; }]);
			$scope.uploadDropdown3.splice(index,1);
			$scope.addBtnName='Add';
			}
		
		

		/* var indexCheck=_.findIndex($scope.gridOptions.data,{'lhskey':$scope.gia.coefficientKey});

		if(indexCheck==-1 ){
			
			 
				$scope.gridOptions.data.push({"variable":$scope.gia.variableValue,"coefficient":$scope.gia.coefficient,"lhskey":$scope.gia.coefficientKey});
				$scope.gia.coefficientKeyValue=$scope.gridOptions.data;
				$scope.gia.coefficient='';
				
				var index= _.findIndex($scope.uploadDropdown3,['lhskey',$scope.gia.coefficientKey])
				
				$scope.uploadDropdown3.splice(index,1);
				$scope.addBtnName='Add';
	}
		else {
			$scope.gridOptions.data[indexCheck].variable=$scope.gia.variableValue;
	    $scope.gridOptions.data[indexCheck].coefficient=$scope.gia.coefficient;
	    $scope.gridOptions.data[indexCheck].lhskey=$scope.gia.coefficientKey;
			$scope.gia.coefficientKeyValue=$scope.gridOptions.data;
			$scope.gia.coefficient='';
			
			var index= _.findIndex($scope.uploadDropdown3,['lhskey',$scope.gia.coefficientKey])
			
			$scope.uploadDropdown3.splice(index,1);
			$scope.addBtnName='Add';
		} */
		
	}


	$scope.startDateFocus=function(){
		if($scope.gia.startDate.length==0){
			$scope.gia.startDate = $filter('date')(new Date(), 'dd/MM/yyyy');
			$scope.startDateValid = false
		   	$scope.endDateValid = false;
		}
	}
	$scope.endDateFocus=function(){
		if($scope.gia.endDate.length==0){
			$scope.gia.endDate = $filter('date')("31/12/2099",'dd/MM/yyyy')
			$scope.startDateValid = false
		   	$scope.endDateValid = false;
		}
	}

						$scope.gridOptions = {
								enableColumnMenus: false,
								enableRowSelection : true,
								multiSelect : false,
								modifierKeysToMultiSelect : false,
								enableColumnMenus : false,
								noUnselect : true,
								enableRowHeaderSelection : false,
														columnDefs : [
																{
																	name : 'Delete',
																	enableSorting : false,
																	cellTemplate : '<button ng-click="grid.appScope.deleteRow(row);$event.stopPropagation();">-</button>'
																},
																{
																	name : 'coefficient',
																	displayName: 'Coefficient'
																},
																{
																	name : 'variable',
																	displayName: 'Variables'
																},
																{
																	name : 'lhskey',
																	displayName: 'Key',
																	visible: false
																} ],
														onRegisterApi : function(
																gridApi) {
															$scope.grid2Api = gridApi;
															$scope.grid2Api.selection.on
																	.rowSelectionChanged(
																			$scope,
																			function(
																					row) {
																				
																				$scope.changeVariable(row.entity);
																				$scope.gia.coefficient=row.entity.coefficient;
																				$scope.index=_.findIndex($scope.gridOptions.data,{'lhskey':row.entity.lhskey});
																				$scope.addBtnName='Edit';
																				
																				
																				if(_.findIndex($scope.uploadDropdown3,{'lhskey':row.entity.lhskey})==-1){
																	        		 $scope.uploadDropdown3.push(row.entity);
																	        		 
																	        		 }
																				
																				

																			});
														}
													};
													$scope.disableDelete = function() {
														if ($scope.gia.id) {
															return false;
														} else {
															return true
														}
													}
													
													$scope.disableDate=true;
													$scope.enableChange = function() {
														if ($scope.gia.isEnabled) {
															$scope.gia.startDate = $filter('date')(new Date(), 'dd/MM/yyyy');
															$scope.gia.endDate = $filter('date')("31/12/2099",'dd/MM/yyyy')
															$scope.startDateValid = false
														   	$scope.endDateValid = false;
															$scope.startDateFocus();
															$scope.endDateFocus();
														} else {
															$scope.gia.startDate = null;
															$scope.gia.endDate = null;
														}
													}
													/* $scope.gridOptions.data = [];/* [
													    {"coefficient":"0.25","variables":"GS1"},
													    {"coefficient":"0.5","variables":"GS2"}
													    ]; */ 
													$scope.gridOptions1 = {};

													$scope.groupChange = function(
															value) {
														$scope.gia.constraintGrpKey = value;

													}
													$scope.populateRow=function(entity){
														$scope.uploadDropdown3.length=0;
														
														$scope.uploadDropdown3=_.cloneDeep(copyVarDropdown);
														$scope.gia = {};
														$scope.index=-2;
														$scope.editedCE=true;
														$scope.duplicateCE=false;
														$scope.gridOptions.data=[];
														$scope.gia.id = entity.id;
														$scope.gia.version = entity.version;
														$scope.gia.eqn = entity.eqn;
														$scope.gia.group = entity.group;
														$scope.gia.desc = entity.desc;
														$scope.gia.startDate = $scope.datesStructure(entity.startDate);
													
														$scope.gia.endDate = $scope.datesStructure(entity.endDate);
														//$scope.gia.coefficient = entity.coefficient;
														$scope.gia.rhs = entity.rhs;
														$scope.gia.comm = entity.comm;
														$scope.gia.coefficientKeyValue=entity.coefficientKeyValue;
														$scope.emptyValue=false;
														 /* $scope.gia.coefficientKeyValue=[{'coefficient':'0.09',
															'lhskey':1,
														'variable':'GIA1'																				
														}
														,{'coefficient':'0.8',
															'lhskey':2,
															'variable':'GIA2'																				
															}]  */
															
															
															
														/* for(var i=0;i<$scope.gia.coefficientKeyValue.length;i++){
															var variableFind= _.filter(entity.coefficientKeyValue,['variable',$scope.gia.coefficientKeyValue[i].variable]);
													var newVariable.push(variableFind)	
														} */
															
															//console.log(variableFind);
															
															$scope.addBtnName='Add';
														$scope.gia.constraintGrpKey=entity.constraintGrpKey;
														$scope.arrayObj = [];
														$scope.arrayObj.lhskey = entity.lhskey;
														$scope.arrayObj.variable = entity.variableName;
														$scope.arrayObj.coefficient = entity.coefficient;
												
														
														
														 for(var i=0;i<$scope.gia.coefficientKeyValue.length;i++){
														$scope.gridOptions.data.push({'variable':$scope.gia.coefficientKeyValue[i].variable,'coefficient':$scope.gia.coefficientKeyValue[i].coefficient,'lhskey':$scope.gia.coefficientKeyValue[i].lhskey});
														 $scope.gridOptions.data =  _.sortBy($scope.gridOptions.data, [function(o) { return o.variable; }]);
														var index= _.findIndex($scope.uploadDropdown3,{'lhskey':$scope.gia.coefficientKeyValue[i].lhskey});
														if(index!==-1){
															$scope.uploadDropdown3.splice(index,1);
															}
														} 
														
																$scope.gia.oper = entity.oper.trim();
														if (entity.isEnabled == 'T') {
															$scope.gia.isEnabled = true;
														} else {
															$scope.gia.isEnabled = false;
														}
														
													}
													$scope.states=function(val){
														return val;
													}
												
												$scope.datesStructure=function(val){
													if(val){
													return val.split('/').reverse().join('/')
													}
												}
													//var newTooltip= '<span tooltips tooltip-view="modal.html" tooltip-side="bottom" tooltip-show-trigger="mouseover" ></span>'
													$scope.gridOptions1 = {
														enableSorting : true,
														enableFiltering : true,
														paginationPageSizes : [ 20,
																40, 60 ],
														paginationPageSize : 20,
														
														//enableFullRowSelection: true,
														enableColumnMenus : false,
														
														 
														columnDefs : [
																{
																	field : 'id',
																	cellTooltip : true,
																	displayName : 'ID',
																	type : 'number',
																	width : '5%' 
																},
																{
																	field : 'version',
																	cellTooltip : true,
																	displayName : 'Version',
																	width : '5%'
																},
																{
																	field : 'eqn',
																	cellTooltip : true,
																	displayName : 'Constraint Name',
																	sort : {
																		direction : 'asc',
																		priority : 0
																	},
																	width : '11.5%',
																	cellTemplate : '<label class="ellipsesCell" ng-click="grid.appScope.populateRow(row.entity);$event.stopPropagation();" title="{{grid.appScope.states(row.entity.eqn)}}">{{grid.appScope.states(row.entity.eqn)}}</label>'


																},
																{
																	field : 'constraintGrpName',
																	cellTooltip : true,
																	displayName : 'Groups',
																	width : '11%'
																},
																{
																	field : 'desc',
																	cellTooltip : true,
																	displayName : 'Description',
																	width : '13%'

																},
																{
																	field : 'lastModifiedBy',
																	cellTooltip : true,
																	displayName : 'Last Modified By',
																	width : '10%'

																},
																{
																	field : 'lastModified',
																	cellTooltip : true,
																	displayName : 'Last Modified On',
																	width : '8%',
																	cellTemplate : '<label class="ellipsesCell" title="{{grid.appScope.datesStructure(row.entity.lastModified)}}">{{grid.appScope.datesStructure(row.entity.lastModified)}}</label>'

																},
																{
																	field : 'createdBy',
																	cellTooltip : true,
																	displayName : 'Created By',
																	width : '7%'

																},
																{
																	field : 'createdTimestamp',
																	cellTooltip : true,
																	displayName : 'Created On',
																	width : '7%',
																	cellTemplate : '<label class="ellipsesCell" title="{{grid.appScope.datesStructure(row.entity.createdTimestamp)}}">{{grid.appScope.datesStructure(row.entity.createdTimestamp)}}</label>'

																},
																{
																	field : 'lastEnabledBy',
																	cellTooltip : true,
																	displayName : 'Last Enabled By',
																	width : '9%'

																},
																{
																	field : 'lastEnabled',
																	cellTooltip : true,
																	displayName : 'Last Enabled',
																	width : '8%',
																	cellTemplate : '<label class="ellipsesCell" title="{{grid.appScope.datesStructure(row.entity.lastEnabled)}}">{{grid.appScope.datesStructure(row.entity.lastEnabled)}}</label>'

																},
																{
																	field : 'lastDisabledBy',
																	cellTooltip : true,
																	displayName : 'Last Disabled By',
																	width : '9%'

																},
																{
																	field : 'lastDisabled',
																	cellTooltip : true,
																	displayName : 'Last Disabled',
																	width : '9%',
																	cellTemplate : '<label class="ellipsesCell" title="{{grid.appScope.datesStructure(row.entity.lastDisabled)}}">{{grid.appScope.datesStructure(row.entity.lastDisabled)}}</label>'

																},
																{
																	field : 'isEnabled',
																	cellTooltip : true,
																	displayName : 'Enable',
																	width : '7%'

																},
																{
																	field : 'startDate',
																	cellTooltip : true,
																	displayName : 'Start Date',
																	width : '7%',
																cellTemplate : '<label class="ellipsesCell" title="{{grid.appScope.datesStructure(row.entity.startDate)}}">{{grid.appScope.datesStructure(row.entity.startDate)}}</label>'

																},
																{
																	field : 'endDate',
																	cellTooltip : true,
																	displayName : 'End Date',
									
			

																width : '7%',
																cellTemplate : '<label class="ellipsesCell" title="{{grid.appScope.datesStructure(row.entity.endDate)}}">{{grid.appScope.datesStructure(row.entity.endDate)}}</label>'

																},
																{
																	field : 'publishStatus',
																	cellTooltip : true,
																	displayName : 'Status?',
																	width : '7%',
																headerCellTemplate: '<div  ng-class="{ \'sortable\': sortable }"><div  class="ui-grid-vertical-bar">&nbsp;</div><div class="ui-grid-cell-contents" col-index="renderIndex"><span title="NEW: New CE* \nEDIT: Edited CE* \nDEL: Deleted CE* \nPUB: Published CE to Operational UI \n*Any CEs that are not in the PUB state means their changes is not reflected in the Operational UI.">{{ col.displayName CUSTOM_FILTERS }}</span><span ui-grid-visible="col.sort.direction" ng-class="{ \'ui-grid-icon-up-dir\': col.sort.direction == asc, \'ui-grid-icon-down-dir\': col.sort.direction == desc, \'ui-grid-icon-blank\': !col.sort.direction }">&nbsp;</span></div><div class="ui-grid-column-menu-button" ng-if="grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false" class="ui-grid-column-menu-button" ng-click="toggleMenu($event)"><i class="ui-grid-icon-angle-down">&nbsp;</i></div><div ng-if="filterable" class="ui-grid-filter-container" ng-repeat="colFilter in col.filters"><input type="text" class="ui-grid-filter-input" ng-model="colFilter.term" ng-click="$event.stopPropagation()" ng-attr-placeholder="{{colFilter.placeholder || \'\'}}" /><div class="ui-grid-filter-button" ng-click="colFilter.term = null"><i class="ui-grid-icon-cancel" ng-show="!!colFilter.term">&nbsp;</i> <!-- use !! because angular interprets \'f\' as false --></div></div></div>'
																} ],
														onRegisterApi : function(
																gridApi) {
															$scope.grid1Api = gridApi;
															
															gridApi.selection.on.rowSelectionChangedBatch($scope,function(row){
																
															
																if(gridApi.selection.getSelectAllState()==false){
																	$scope.gia={};
																	$scope.gridOptions.data=[];
																	$scope.addBtnName='Add';
																	$scope.submitted=false;
																	$scope.editedCE=false;
																	$scope.clickedRow=true;
																	Count=row.length;
																	$scope.uploadDropdown3.length=0;
																	$scope.uploadDropdown3=_.cloneDeep(copyVarDropdown);
																
																_.forEach(row, function(obj) {
																	rowId.push(obj.entity);
																})
																$scope.publishData=rowId;
																
																
																}else{
																	$scope.clickedRow=false;
																	Count=0;
																_.forEach(row, function(obj) {
																_.remove(rowId, function (item) {
															  		return item.id == obj.entity.id;
																	});
																});
																$scope.publishData=rowId;
																}
																});
															
															gridApi.selection.on
																	.rowSelectionChanged(
																			$scope,
																			function(
																					row) {
																				if(row.isSelected==true){
																					$scope.uploadDropdown3.length=0;
																					$scope.uploadDropdown3=_.cloneDeep(copyVarDropdown);
																				rowId.push(row.entity);
																				Count++;
																			$scope.publishData=rowId;
																			if($scope.gia){
																				$scope.gia={};
																				$scope.gridOptions.data=[];
																				$scope.addBtnName='Add';
																				$scope.submitted=false;
																				$scope.editedCE=false;
																				//$scope.clickedRow=true;
																			}
																				}
																				else{
																				
																				//_.forEach(row.entity, function(obj) {
																					_.remove(rowId, function (item) {
																				  		return item.id == row.entity.id;
																						});
																					$scope.publishData=rowId;
																				//})
																					Count--;
																					
																				}
																				 if(Count>0){
																					 $scope.clickedRow=true;
																				} else
																					$scope.clickedRow=false;

																			});
														}
													};

												} 
			
		 ]);
