/*
 * Service to send data object to retrieve search documents.
 * 
 */
Gia.service('logoutService', [ '$http', '$q', function($http, $q) {
	this.logout = function() {
		var deferred = $q.defer();
		$http.get('/gia/logout').success(function(data) {
			deferred.resolve(data);
		}).error(function(error) {
			deferred.reject(error);
		});

		return deferred.promise;
	}

} ]);