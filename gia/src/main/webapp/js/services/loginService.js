/*
 * Service to send data object to retrieve search documents.
 * 
 */
Gia.service('loginService', [ '$http', '$q', function($http, $q) {
	this.getData = function(credential) {
		var deferred = $q.defer();
		$http.post('../web/getlogin', credential).success(function(data) {
			
			deferred.resolve(data);
		}).error(function(error) {
			console.log("Error: " + error);
			deferred.reject(error);
		});

		return deferred.promise;
	}

} ]);