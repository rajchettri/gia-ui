'use strict';

/**
 * @ngdoc service
 * @name giaAngularApp.PlatformApi
 * @description
 * # PlatformApi
 * Factory in the giaAngularApp.
 */
Gia.factory('PlatformApi', function($resource) {
  var API_URL_MOCK = "http://localhost:3000/mirrors/:mirrorKey";
  var API_URL_LOCAL_STACK = __env.serverUrl + "referencedata/platform/:mirrorKey";
  var API_URL_DEV = "http://localhost:3000/platform/:mirrorKey";


  return $resource(API_URL_LOCAL_STACK, {
    mirrorKey: '@mirrorKey'
  }, {
    update: {
      method: 'PUT'
    }
  });
});
