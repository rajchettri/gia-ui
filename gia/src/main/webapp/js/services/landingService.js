/*
 * Service to send data object to retrieve search documents.
 * 
 */
Gia.service('landingService', [ '$http', '$q', function($http, $q) {
	this.getData = function(user) {
		var deferred = $q.defer();
		$http.get('/gia/userdetails').success(function(data) {

			deferred.resolve(data);
		}).error(function(error) {
			deferred.reject(error);
		});

		return deferred.promise;
	}

} ]);