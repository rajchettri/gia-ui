Gia.service('constraintMService', [
		'$http',
		'$q',
		function($http, $q) {
			
			this.getData = function(gia) {
				var deferred = $q.defer();
				
				gia.startDate1 = gia.startDate;
				gia.endDate1 = gia.endDate;
				gia.startDate1=gia.startDate1.split('/').reverse().join('/');
				gia.endDate1= gia.endDate1.split('/').reverse().join('/');
				$http.post('/gia/save',gia).success(function(data) {
					deferred.resolve(data);

				})
				.error(function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}

			this.getDeleteCEPushed = function(Id) {
				var deferred = $q.defer();
				$http.post('/gia/delete', Id)
						.success(function(data) {
							deferred.resolve(data);
						}).error(function(error) {
							deferred.reject(error);
						});

				return deferred.promise;
			}

			this.onLoad = function() {
				var deferred = $q.defer();
				$http.get('/gia/load')
						.success(function(data) {
							deferred.resolve(data);
						}).error(function(error) {
							deferred.reject(error);
						});

				return deferred.promise;
			}
		} ]);