'use strict';

/**
 * @ngdoc service
 * @name giaAngularApp.GroupApi
 * @description
 * # GroupApi
 * Factory in the giaAngularApp.
 */
Gia.factory('GroupApi', function($resource) {
  var API_URL_MOCK = "http://localhost:3000/groups/:constraintGroupKey";
  var API_URL_LOCAL_STACK = __env.serverUrl + "/referencedata/group/:constraintGroupKey";
  var API_URL_DEV = "http://localhost:3000/groups/:constraintGroupKey";


  return $resource(API_URL_LOCAL_STACK, {
    constraintGroupKey: '@constraintGroupKey'
  }, {

    create: {
      method: 'POST'
    }
  });
});
