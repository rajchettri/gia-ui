'use strict';

/**
 * @ngdoc service
 * @name giaAngularApp.variable
 * @description
 * # variable
 * Factory in the giaAngularApp.
 */
Gia.factory('VariableApi', function($resource) {
    var API_URL_MOCK = "http://localhost:3000/variable/:variableKey";
    var API_URL_LOCAL_STACK = __env.serverUrl + "referencedata/variable/:variableKey";
    var API_URL_DEV = "http://localhost:3000/variable/:variableKey";
    return $resource(API_URL_LOCAL_STACK, {
      variableKey: '@variableKey'

    }, {
      update: {
        method: 'PUT' // this method issues a PUT request
      },
      get: {
        isArray: false
      },
      query: {
        isArray: true
      }
    });
  });
